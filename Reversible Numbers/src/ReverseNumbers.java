/*
Author: Jaime Lopez-Merizalde
Date: 02 Jan 2021
UpD: 04 Jan 2021
Descri:
https://projecteuler.net/problem=145
 */

public class ReverseNumbers {

    /*
    Fields
     */
    private static int totalReversibleNumbers = 0;
    private int flippedAddedNumber;

    /*
    Getters and Setters
     */
    public int getFlippedAddedNumber(){ return flippedAddedNumber; };
    public int getTotalReversibleNumbers(){ return totalReversibleNumbers; };

    /*
    Methods
     */

    //Puts digits in left to right order as they appear
    private int[] NumberDigitzer(int number) {
        final int DIGIT_TOL = NumberDigitsNeeded(number);

        int[] digitizedNumber = new int[DIGIT_TOL];

        int idxDigit = 0;
        while (idxDigit < DIGIT_TOL) {
            digitizedNumber[idxDigit]
                    = (int) ((number % Math.pow(10, DIGIT_TOL - idxDigit) - number % Math.pow(10, DIGIT_TOL - idxDigit - 1)) / Math.pow(10, DIGIT_TOL - idxDigit - 1));
            idxDigit++;
        }
        return digitizedNumber;
    }

    private int[] NumberFlipper(int[] digitizedNumber) {

        //flip the number
        int[] flippedNumber = new int[digitizedNumber.length];

        //Flip the number
        for (int idx1 = 0; idx1 < digitizedNumber.length; idx1++) {
            flippedNumber[idx1] = digitizedNumber[digitizedNumber.length - idx1 - 1];
        }

        return flippedNumber;
    }

    private boolean IsReversible(int[] digitizedNumber){

        boolean isReversible = true;
        int idxDigit = 0;

        while(isReversible && idxDigit <digitizedNumber.length ){
            if (digitizedNumber[idxDigit]%2==0)
                isReversible = false;

            idxDigit++;
        }
        return isReversible;
    }

    private int UnDigitize(int[] digitizedNumber) {

        int undigitizedNumber = 0;

        for (int idx = 0; idx < digitizedNumber.length; idx++) {
            undigitizedNumber = (int) (undigitizedNumber + Math.pow(10, idx) * digitizedNumber[digitizedNumber.length - 1 - idx]);
        }
        return undigitizedNumber;

    }

    private int NumberDigitsNeeded(int number) {

        int digitsNeeded = 1;

        while (number - Math.pow(10, digitsNeeded) >= 0) {
            digitsNeeded++;
        }

        return digitsNeeded;
    }

    public boolean ReverseNumberProcess(int number){
        //check number for leading zeros here
        int[] digitizedNumber = NumberDigitzer(number);

        if (digitizedNumber[0]!=0 && digitizedNumber[digitizedNumber.length-1]!=0) {
            flippedAddedNumber = UnDigitize(NumberFlipper(digitizedNumber)) + number;

            if (IsReversible(NumberDigitzer(flippedAddedNumber))) {
                totalReversibleNumbers++;
                return true;
            } else {
                return false;
            }
        }
        else
            return false;
    }
}


