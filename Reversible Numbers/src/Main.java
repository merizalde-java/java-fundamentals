
/*
Author: Jaime Lopez-Merizalde
Date: 31 Dec 2020
UpD: 06 Jan 2021
Descri: Finds number of reversible numbers belo 10^9
https://projecteuler.net/problem=145
 */
public class Main {

    public static void main(String[] args) {
//        testReversibleProblem();
        ReversibleProblem();
    }

    /*
    Methods
     */
    //First Testing Class
    private static void testReversibleProblem(){

        /*
            sample data here
            Data types: integers
            Numbers like 10, 200 should fail because they are
            1001 should fail because it's reverse has leading zeros
            0002 also has leading zeros
         */
        //int[] numbersToTest = {1, 2, 10, 15, 27, 200, 1001, 0002};
        /*
        This MathTools Class should be somehow imported as a reusable Library.
         */
        int[] numbersToTest = MathTools.consecutiveIntegers(1, 999);

        //Instantiate new class
        ReverseNumbers ReverseProcess = new ReverseNumbers();

        for( int idx1 = 0; idx1 < numbersToTest.length; idx1++){
            boolean isReversible = ReverseProcess.ReverseNumberProcess(numbersToTest[idx1]);
            System.out.println("Is " + numbersToTest[idx1] + " reversible: " + isReversible + ".");
            if ( isReversible ){
                System.out.println("        Flipped and Added: " + ReverseProcess.getFlippedAddedNumber());
            }
        }
        System.out.println("How many numbers are reversible? " + ReverseProcess.getTotalReversibleNumbers());
    }

    private static void ReversibleProblem(){

        //Instantiate new class
        ReverseNumbers ReverseProcess = new ReverseNumbers();

        for (int idx2 = 1; idx2< (int) Math.pow(10,9); idx2++){
           ReverseProcess.ReverseNumberProcess(idx2);
        }

        System.out.println("How many numbers are reversible? " + ReverseProcess.getTotalReversibleNumbers());
    }
}