/*
Date: 21 Nov 20
UpD: 30 Nov 20
Descri: makes triangle which houses vertices. Determines if origin is contained in triangle
 */

public class Triangle {

    final public Vertex VERTEX_A;
    final public Vertex VERTEX_B;
    final public Vertex VERTEX_C;
    private static int triangleOriginTracker = 0;
    private final Vertex[] ORDERED_VERTICES;

    /*
    Constructors
     */
    //Overloaded constructor: accepts multiple Vertex objects.
    public Triangle(Vertex VERTEX_A, Vertex VERTEX_B, Vertex VERTEX_C){
        Vertex[] orderedVertices = OrderVertices(new Vertex[] {VERTEX_A, VERTEX_B, VERTEX_C});

        this.ORDERED_VERTICES = orderedVertices;
        this.VERTEX_A = orderedVertices[0];
        this.VERTEX_B = orderedVertices[1];
        this.VERTEX_C = orderedVertices[2];

        NonColinearCheck();

    }

    //Overloaded constructor: accepts String array of vertices
    public Triangle(String[] vertices){
        this(new Vertex(Integer.parseInt(vertices[0]), Integer.parseInt(vertices[1])),
                new Vertex(Integer.parseInt(vertices[2]), Integer.parseInt(vertices[3])),
                new Vertex(Integer.parseInt(vertices[4]), Integer.parseInt(vertices[5])));
    }

    /*
    Getters
     */
    public String[] getOrderedVertices(){
        return new String[]{VERTEX_A.getCoordinatePair()[0],VERTEX_A.getCoordinatePair()[1],
                            VERTEX_B.getCoordinatePair()[0],VERTEX_B.getCoordinatePair()[1],
                            VERTEX_C.getCoordinatePair()[0],VERTEX_C.getCoordinatePair()[1]};
    }
    public int getTriangleOriginTracker(){ return triangleOriginTracker;}

    /*
    Methods
     */
    //non-colinear check
    public boolean NonColinearCheck() {
        //coordinates for setting up the line
        double xCoord1 = VERTEX_A.X_COORDINATE;
        double yCoord1 = VERTEX_A.Y_COORDINATE;
        double xCoord2 = VERTEX_B.X_COORDINATE;
        double yCoord2 = VERTEX_B.Y_COORDINATE;
        double xCoord3 = VERTEX_C.X_COORDINATE;
        double yCoord3 = VERTEX_C.Y_COORDINATE;


        //Scene 1: nonvertial line (probabilistically more likely to happen).
        if (xCoord1 != xCoord2) {
            double m = ((double) (yCoord2 - yCoord1)) / ((double) (xCoord2 - xCoord1));

            //Check if the m is actually too large for a double
            if (m > Double.MAX_VALUE - 2 || m < -Double.MAX_VALUE + 2) {
                System.out.println(String.format("Values: %d " +
                        "and %d are too similar and result in very " +
                        "large double that cannot be handled", xCoord1, xCoord2));
            }

            if ((xCoord2 - xCoord1) * (yCoord3 - yCoord1) == (yCoord2 - yCoord1) * (xCoord3 - xCoord1)) {
                System.out.println("Degenerate Triangle: Colinear");
                return false;
            } else {
                return true;
            }

            //Scene 2: one line is vertical.
        } else {
            if(yCoord1 == yCoord2) {
                System.out.println("Degenerate Triangle: duplicate vertices");
                return false;
            }
            else if (xCoord1 == xCoord3) {
                System.out.println("Degenerate Triangle: Colinear");
                return false;
            }
            else{
                return true;
            }
        }
    }

    private Vertex[] OrderVertices(Vertex[] vertices){
        //assume you don't have colinearity

        //order by left-most and counterclockwise
        Vertex[] OrderedVertexArray = vertices;

        //Do left most
        for(int idxCompare = 1; idxCompare < OrderedVertexArray.length; idxCompare++){
            if ( OrderedVertexArray[0].X_COORDINATE > OrderedVertexArray[idxCompare].X_COORDINATE ){
                //switch around
                Vertex vertexTemp = OrderedVertexArray[0];
                OrderedVertexArray[0] = OrderedVertexArray[idxCompare];
                OrderedVertexArray[idxCompare] = vertexTemp;
            } //otherwise the comparison reached an equality

            else if ( OrderedVertexArray[0].X_COORDINATE == OrderedVertexArray[idxCompare].X_COORDINATE ){
                //IF WE HAVE A VERTICAL LINE AS THE 'LEFTMOST' THEN TAKE THE LOWEST Y VALUE AS THE LEFTMOST VERTEX
                if ( OrderedVertexArray[0].Y_COORDINATE > OrderedVertexArray[idxCompare].Y_COORDINATE ){
                    //switch around
                    Vertex vertexTemp = OrderedVertexArray[0];
                    OrderedVertexArray[0] = OrderedVertexArray[idxCompare];
                    OrderedVertexArray[idxCompare] = vertexTemp;
                }
            }
        }
        //Do next point
        int[] vector1 = {OrderedVertexArray[1].X_COORDINATE - OrderedVertexArray[0].X_COORDINATE,
                    OrderedVertexArray[1].Y_COORDINATE - OrderedVertexArray[0].Y_COORDINATE};

        int[] vector2 = {OrderedVertexArray[2].X_COORDINATE - OrderedVertexArray[0].X_COORDINATE,
                OrderedVertexArray[2].Y_COORDINATE - OrderedVertexArray[0].Y_COORDINATE};

        //Do a Left-hand check
        if ( vector1[0]*vector2[1] - vector1[1]*vector2[0] <= 0 ){
            //switch around
            Vertex vertexTemp = OrderedVertexArray[1];
            OrderedVertexArray[1] = OrderedVertexArray[2];
            OrderedVertexArray[2] = vertexTemp;
        }

        return OrderedVertexArray;
    }

    public boolean ContainsOrigin(){

        //HAS TO pass noncolinear check
        if ( NonColinearCheck() ){
            /*
            The way this does and does not pass the check is by notpassing the left hand counter-clockwise cross product;
            If a point is inside a n-gon, the cross product of the i-th ray (counterclockwise) of every 2 consecutive
            points in the n-gon and the projection of the point onto this ray has to be positive. IE, it always has to be
            on the left hand side of the CCW rays of the n-gon. Otherwise, it is on the outside of the n-gon
             */
            int idxRay = 0;
            boolean containsOrigin = false;
            //has to take ordered vertices
            while ( LeftHandCheck(ORDERED_VERTICES[idxRay%3], ORDERED_VERTICES[(idxRay+1)%3]) && idxRay< 3 ){

                //If all edges pass the left-hand rule check, we contain the origin.
                if ( idxRay == 2 ){

                    containsOrigin = true;
                    //Increment the triangle origin tracker.
                    triangleOriginTracker++;
                    break;
                }
                idxRay++;
            }
            return containsOrigin;

        } //if it doesn't then just return false by failing the noncolinear check.
        else {
            System.out.println("Triangle did not pass nonlinear check!");
            return false;
        }
    }

    private boolean LeftHandCheck(Vertex vertex1, Vertex vertex2) {
        //vertex1 and vertex2 should be ordered by counterclockwise
        Vertex A = vertex1;
        Vertex B = vertex2;

        //First if handles Verticality
        if( A.X_COORDINATE == B.X_COORDINATE){
            if ( A.X_COORDINATE > 0 ){ return true;}
            else { return false; }
        }
        else {
            //Warning: IF the denominator is too low, this will result in an very large value for m which could place it
            // out of reach for a double.
            double m = ((double) (B.Y_COORDINATE - A.Y_COORDINATE))/((double) (B.X_COORDINATE - A.X_COORDINATE));

            //Check if the m is actually too large for a double
            if (m > Double.MAX_VALUE - 2 || m < -Double.MAX_VALUE + 2) {
                System.out.println(String.format("Values: %d " +
                        "and %d are too similar and result in very " +
                        "large double that cannot be handled", A.X_COORDINATE, B.X_COORDINATE));
                return false;
            }

            //The reason to do a calculation this way is to avoid multiplying large numbers
            //we only care about the +, - sign, not the actual value so we use bools instead.
            boolean sStarBoolean = m*A.X_COORDINATE - A.Y_COORDINATE > 0? true: false;
            boolean cStarBoolean = B.X_COORDINATE - A.X_COORDINATE + m*(B.Y_COORDINATE - A.Y_COORDINATE) > 0? true: false;

            if ( sStarBoolean == cStarBoolean ){ return true; }
            else { return false; }
        }
    }

}
