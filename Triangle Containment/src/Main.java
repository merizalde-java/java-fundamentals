/*
Author: Jaime Lopez
Date: 21 Nov 2020
UpD: 30 Nov 2020
Descri: Takes three random integer data points in [-1000,1000]^2 and finds if the origin is
contained in the triangle they form.
 */

import java.io.FileNotFoundException;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        //load the data
        String currentDir = System.getProperty("user.dir");
        String dataFileName = currentDir + "/data/p102_triangles.txt";
        String saveFileName = currentDir + "/output/p102_trianges_solution.txt";

//        //select sample data
//        TestTriangleContainment(dataFileName);

        //Count Overall triangles with origin from Data file.
//        TriangleContainment(dataFileName);

        //Count Overall triangles with origin from Data file and save file.
        try{
            TriangleContainment(dataFileName, saveFileName);
        } catch (FileNotFoundException e){
            System.out.println("File Not Found Exception: " + saveFileName);
        }

    }

    private static void TestTriangleContainment(String dataFileName) {

        int[] idxArray = {0, 1, 59 };

        DataHandler dataHandler = new DataHandler(dataFileName);
        String[][] dataArray = dataHandler.DataReader();

        //instantiate a triangle with real data
        for (int idxLine : idxArray) {
            Triangle triangle = new Triangle(dataArray[idxLine]);
            System.out.println("Line: " + idxLine);
            System.out.println("Coordinates, before reorder: " + Arrays.toString(dataArray[idxLine]));
            System.out.println("Coordinates, after ordering: " + Arrays.toString(triangle.getOrderedVertices())
                    + " Has origin: " + triangle.ContainsOrigin());
            System.out.println();
        }

        System.out.println("How many triangles have the origin? "
                + new Triangle(dataArray[0]).getTriangleOriginTracker());

    }

    //Overloaded: method which only calculates the result
    private static void TriangleContainment(String dataFileName) {
        DataHandler dataHandler = new DataHandler(dataFileName);
        String[][] dataArray = dataHandler.DataReader();

        //start counting triangles w origin in them.
        for(int idxLine = 0; idxLine < dataArray.length; idxLine++){
           new Triangle(dataArray[idxLine]).ContainsOrigin();
        }

        System.out.println("How many triangles have the origin? "
                + new Triangle(dataArray[0]).getTriangleOriginTracker());
    }

    //Overloaded: method which saves to a file
    private static void TriangleContainment(String dataFileName, String saveFileName) throws FileNotFoundException {

        //Instantiate a data handler and data array.
        DataHandler dataHandler = new DataHandler(dataFileName);
        String[][] dataArray = dataHandler.DataReader();

        //Instantiate a boolean array storing true false if triangle contains origin or not.
        int numberDataLines = dataArray.length;
        boolean[] containsOriginArray = new boolean[numberDataLines];

        //Perform contains origin calculations.
        for (int idxLine = 0; idxLine < numberDataLines; idxLine++) {
            containsOriginArray[idxLine] = new Triangle(dataArray[idxLine]).ContainsOrigin();
        }

        //Use data handler DataWriter() method to write to a text file using the saveFileName.
        dataHandler.DataWriter(saveFileName, dataArray, containsOriginArray);
        System.out.println("Results saved at: " + saveFileName);

        //Print main result to the comman line.
        System.out.println("How many triangles have the origin? "
                + new Triangle(dataArray[0]).getTriangleOriginTracker());
    }

}
