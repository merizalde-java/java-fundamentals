import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

/*
Author: Jaime Lopez
Date: 5 Nov 2020
UpD: 30 Nov 2020
Descri: Provides String input and data digestion
/*
Notes: 24 Nov 2020: added writing capability
NOTES: the data formatter is what needs to be changed each time we adapt the problem.
 */
public class DataHandler {

    /*
    Fields
     */
    //Defines the space character as the separator
    private String dataFileName;

    /*
    getters
     */
    public String getDataFileName(){
        return dataFileName;
    }

    /*
    Constructors
     */
    public DataHandler(){};

    public DataHandler(String dataFileName){
        this.dataFileName = dataFileName;
    };

    /*
    MEthods
     */
    public String[][] DataReader(){
        //So we can call it with the data File it was constructed with
        String[] dataArray0 = DataReader(this.dataFileName);
        return DataStringFormatter(dataArray0);
    }

    //reads in the data
    public String[] DataReader(String dataFileName){

        int numberFileLines = 1;

        //Declare instance of new file
        File dataFile = new File(dataFileName);

        //DIAG
        System.out.println("Data File: " + dataFile);

        try {
            numberFileLines = NumberFileLines(dataFile);
            System.out.println("Number of Lines in the File is " + numberFileLines);
        } catch(IOException e){
            System.out.println("Could not read file: " + dataFileName);
        }

        String[] dataArray0 = new String[numberFileLines];

        //Scanner can work on objects of class File
        try {
            Scanner dataScanner = new Scanner(dataFile);
            int lineCounter = 0;

            while (dataScanner.hasNextLine() && lineCounter<numberFileLines ) {
                try {
                    dataArray0[lineCounter] = dataScanner.next();
                }catch (Exception e){
                    System.out.println("Line " + lineCounter + "has a potential issue being read in.");
                }
                lineCounter++;
            }
            dataScanner.close();
        } catch(FileNotFoundException e){
            System.out.println("Could not find the file: " + dataFileName);
        }
        return dataArray0;
    }

    //writes the data
    public void DataWriter(String saveFileName, String[][] dataArray, boolean[] containsOrigin) throws FileNotFoundException {

        int numberDataLines = dataArray.length;

        PrintWriter out = new PrintWriter(saveFileName);

        for (int idxLine = 0; idxLine < numberDataLines; idxLine++) {
            out.println(idxLine + " " + Arrays.toString(dataArray[idxLine]) + " contains origin: " + containsOrigin[idxLine]);
        }

        //write the final line with the totals:
        out.println(numberDataLines + " How many triangles have the origin? " + new Triangle(dataArray[0]).getTriangleOriginTracker());
        out.close();
    }

    //Multidimensional array of formatted dataArray0
    public String[][] DataStringFormatter(String[] dataArray0) {

        //declare formatted dataArray of ?x2 dimensions
        String[][] dataArray = new String[dataArray0.length][1];

        //LOOP THROUGH AND READ
        //loop through all file lines
        for (int idxLine = 0; idxLine < dataArray0.length; idxLine++) {
            dataArray[idxLine] = dataArray0[idxLine].split(",");
        }

        return dataArray;
    }

    public int NumberFileLines(File file) throws IOException {
        //https://tinyurl.com/yxhxscm9
        FileInputStream fis = new FileInputStream(file);
        byte[] byteArray = new byte[(int) file.length()];
        fis.read(byteArray);
        String data = new String(byteArray);
        String[] stringArray = data.split("\n");
        return stringArray.length;
    }

}
