/*
Date: 21 Nov 20
UpD: 21 Nov 20
Descri: makes vertices based on string
 */

public class Vertex {

    final public int X_COORDINATE;
    final public int Y_COORDINATE;

    /*
    Constructors
     */
    public Vertex(int X_COORDINATE, int Y_COORDINATE ){
        this.X_COORDINATE = X_COORDINATE;
        this.Y_COORDINATE = Y_COORDINATE;
    };

    //construct w coordinates
    public Vertex(String[] coordinates){
        this(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1]));
    }

    /*
    Getters
     */
    public String[] getCoordinatePair(){
        return new String[]{Integer.toString(X_COORDINATE), Integer.toString(Y_COORDINATE)};
    }

}
