/*
Author: Jaime Lopez
Date: 9 Nov 2020
UpD: 12 Nov 2020
Descri: Class of Poker PLayer
 */
/*
Notes: 12 Nov 2020: developing
 */


import java.util.Arrays;

public class PokerPlayer implements Comparable<PokerPlayer>{

    private String playerName;
    private PokerHand playerHand;

    /*
    Getters
     */
    public String getPlayerName(){
        return this.playerName;
    }
    public String getPlayerCards(){
        return this.playerHand.getPokerHandToString();
    }
    public String getPlayerHighCard(){
        return String.valueOf(this.playerHand.getHighCard().getCardValue());
    }
    public String getPlayerHandRank() {
        return playerHand.getHandRank().toString();
    }

    /*
    Setters
     */
    public void setPlayerName(String playerName){
        this.playerName = playerName;
    }
    //Overloaded: String[]
    public void setPlayerHand(String[] Cards){
        playerHand = new PokerHand(Cards);
    }
    //Overloaded: Card[]
    public void setPlayerHand(Card[] Cards){
        playerHand = new PokerHand(Cards);
    }

    /*
    Constructors
     */
    public PokerPlayer(){};
    public PokerPlayer(String name){
        this.playerName = name;
    }
    public PokerPlayer(String[] Cards){
        playerHand = new PokerHand(Cards);
        playerHand.FindHandRank();
    }
    public PokerPlayer(String name, String[] Cards){
        this(Cards);
        this.playerName = name;
    }

    public void FindPlayerHandRank() {
        playerHand.FindHandRank();
    }

    @Override
    public int compareTo(PokerPlayer pokerPlayer) {

        return playerHand.compareTo(pokerPlayer.playerHand);
    }
}
