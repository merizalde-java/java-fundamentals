import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/*
Author: Jaime Lopez
Date: 5 Nov 2020
UpD: 5 Nov 2020
Descri: Provides String input and data digestion
 */
/*
Notes: 5 Nov 2020 you need to test the reader
 */
public class DataHandler {

    /*
    Fields
     */
    //Defines the space character as the separator
    public final int HAND_COUNT = 5;
    public final int CARD_SIGNATURE = 2;
    public final int PLAYERS = 2;
    private String dataFileName;

    /*
    getters
     */
    public String getDataFileName(){
        return this.dataFileName;
    }

    /*
    Constructors
     */
    public DataHandler(){};

    public DataHandler(String dataFileName){
        this.dataFileName = dataFileName;
    };

    /*
    MEthods
     */

    //reads in the data
    //OVERLADED!
    public String[][][] DataReader(){
        //So we can call it with the data File it was constructed with
        String[] dataArray0 = DataReader(this.dataFileName);
        return DataStringFormatter(dataArray0);
    }
    public String[] DataReader(String dataFileName){

        int numberFileLines = 1;

        //Declare instance of new file
        File dataFile = new File(dataFileName);

        //DIAG
        System.out.println(dataFile);

        try {
            numberFileLines = NumberFileLines(dataFile);
            System.out.println("Number of Lines in the File is " + numberFileLines);
        } catch(IOException e){
            System.out.println("Could not read file: " + dataFileName);
        }

        String[] dataArray0 = new String[numberFileLines];

        //Scanner can work on objects of class File
        try {
            Scanner dataScanner = new Scanner(dataFile);
            int lineCounter = 0;

            while (dataScanner.hasNextLine() && lineCounter<1000 ) {
                try {
                    dataArray0[lineCounter] = dataScanner.nextLine();
                }catch (Exception e){
                    System.out.println("Line " + lineCounter + "has a potential issue being read in.");
                }
                lineCounter++;
            }
            dataScanner.close();
        } catch(FileNotFoundException e){
            System.out.println("Could not find the file: " + dataFileName);
        }
        return dataArray0;
    }

    //Multidimensional array of formatted dataArray0
    public String[][][] DataStringFormatter(String[] dataArray0){
        //declare formatted dataArray of ?x2 dimensions
        String[][][] dataArray = new String[dataArray0.length][PLAYERS][HAND_COUNT];

        //LOOP THROUGH AND READ
        //loop through all file lines
        for(int idxLine = 0; idxLine<dataArray0.length; idxLine++){

            //on each file line: start populating the player cards
            for(int idxPlayer = 0; idxPlayer<PLAYERS; idxPlayer++){

                for(int idxCard = 0; idxCard< HAND_COUNT; idxCard++){

                    //idxCard take the first of each index which is alwys the "3rd" character down the line starting with 0
                    char cardNumValue = dataArray0[idxLine].charAt(idxCard*(CARD_SIGNATURE+1) + idxPlayer*HAND_COUNT*(CARD_SIGNATURE+1));
                    //idxCard take the first of each index which is alwys every 3rd character down the line starting with 1
                    char cardSuite = dataArray0[idxLine].charAt(idxCard*(CARD_SIGNATURE+1)+1 + idxPlayer*HAND_COUNT*(CARD_SIGNATURE+1));

                    dataArray[idxLine][idxPlayer][idxCard] = String.format("%c%c", cardNumValue, cardSuite);
                }
            }
        }

        return dataArray;
    }

    public int NumberFileLines(File file) throws IOException {
        //https://tinyurl.com/yxhxscm9
        FileInputStream fis = new FileInputStream(file);
        byte[] byteArray = new byte[(int) file.length()];
        fis.read(byteArray);
        String data = new String(byteArray);
        String[] stringArray = data.split("\n");
        return stringArray.length;
    }

}
