import java.io.File;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.String.valueOf;

public class Main {
    /*
    Author: Dr. Jaime Lopez-Merizalde
    Date Start: 4 Nov 20
    Date Update: 13 Nov 20
    Descri: counts winning hands of poker players
    Web:
     */

    public static void main(String[] args) {

        String currentDir = System.getProperty("user.dir");
        String dataFileName = currentDir + "/data/pokerHands.txt";

        String[][][] dataArrayFormatted = HandleSomeData(dataFileName);
        System.out.println("Sample line of data: " + Arrays.toString(dataArrayFormatted[0][0]));

        PokerPlayer Shayla = new PokerPlayer("Shayla");
        PokerPlayer Jaime = new PokerPlayer("Jaime");

        System.out.println("Poker Players: " + Shayla.getPlayerName() + " " + "vs" + " " + Jaime.getPlayerName());

//        String[] resultsLines = compareHands(25, dataArrayFormatted, Shayla, Jaime);
//        for (int idxLines = 0 ; idxLines < resultsLines.length; idxLines++){
//            System.out.println(resultsLines[idxLines]);
//        }

        String[] resultsLines = compareHands( dataArrayFormatted, Shayla, Jaime);
        for (int idxLines = 0 ; idxLines < resultsLines.length; idxLines++){
            System.out.println(resultsLines[idxLines]);
        }

    }

    /*
    Methods
     */

    private static String[][][] HandleSomeData(String dataFileName){
        //instantiate an instance of the dataHandler
        DataHandler dataHandler = new DataHandler();
        String[] dataArray0 = dataHandler.DataReader(dataFileName);

        String[][][] dataArrayFormatted = dataHandler.DataStringFormatter(dataArray0);

        return dataArrayFormatted;
    }

    public static Card MakeACard(String cardString){
        return new Card(cardString);
    }

    //As a string
    public static PokerHand MakeAPokerHand(String[] pokerHandString){

        //Turn the string array into card array objects
        Card[] cardArray = new Card[pokerHandString.length];
        for(int idx=0; idx<pokerHandString.length; idx++){
            cardArray[idx] = MakeACard(pokerHandString[idx]);
        }

        //return instantiated array
        return new PokerHand(cardArray);
    }

    public static String[] compareHands(String[][][] dataArrayFormatted, PokerPlayer player1, PokerPlayer player2){

        int  player1Wins = 0, player2Wins = 0, totalTies = 0;

        int totalLines = dataArrayFormatted.length;
        String[] resultsLines = new String[totalLines];

        //randomly select a certain number of lines from the possible available lines
        for(int idxLines = 0; idxLines < totalLines; idxLines++){

            //dataArrayFormatted[dataArray0.length][PLAYERS]
            player1.setPlayerHand(dataArrayFormatted[idxLines][0]);
            player2.setPlayerHand(dataArrayFormatted[idxLines][1]);

            player1.FindPlayerHandRank();
            player2.FindPlayerHandRank();

            String result;
            if (player1.compareTo(player2)>0) {
                result = player1.getPlayerName();
                player1Wins++;
            }
            else if (player1.compareTo(player2) < 0 ){
                result = player2.getPlayerName();
                player2Wins++;
            }
            else {
                totalTies++;
                result = "both players have same hand!";
            }

            resultsLines[idxLines] = String.format("For game %d, with hands: %s %s %s and %s %s %s, the winner is %s, ",
                    idxLines, player1.getPlayerCards(),player1.getPlayerHandRank(), player1.getPlayerHighCard(), player2.getPlayerCards(),player2.getPlayerHandRank(), player2.getPlayerHighCard(), result);
        }

        System.out.println(String.format("Player 1 (%s) wins %d times!",player1.getPlayerName(), player1Wins ));
        System.out.println(String.format("Player 2 (%s) wins %d times!",player2.getPlayerName(), player2Wins ));
        System.out.println(String.format("Total Ties %d times!", totalTies));
        return resultsLines;

    }
    public static String[] compareHands(int randomSelection, String[][][] dataArrayFormatted, PokerPlayer player1, PokerPlayer player2){
        int  player1Wins = 0, player2Wins = 0, totalTies = 0;

        Random r = new Random();
        int totalLines = dataArrayFormatted.length;
        String[] resultsLines = new String[randomSelection];

        //randomly select a certain number of lines from the possible available lines
        for(int idxLines = 0; idxLines < randomSelection; idxLines++){

            int random = r.nextInt(totalLines-1);

            //dataArrayFormatted[dataArray0.length][PLAYERS]
            player1.setPlayerHand(dataArrayFormatted[random][0]);
            player2.setPlayerHand(dataArrayFormatted[random][1]);

            player1.FindPlayerHandRank();
            player2.FindPlayerHandRank();

            String result;
            if (player1.compareTo(player2)>0){
                result = player1.getPlayerName();
                player1Wins++;
            }
            else if (player1.compareTo(player2) < 0 ){
                result = player2.getPlayerName();
            }
            else {
                result = "both players have same hand!";
            }

            resultsLines[idxLines] = String.format("For game %d, with hands: %s %s %s and %s %s %s, the winner is %s, " +
                            "with corresponding line in the data array: %d",
                    idxLines, player1.getPlayerCards(),player1.getPlayerHandRank(), player1.getPlayerHighCard(), player2.getPlayerCards(),player2.getPlayerHandRank(), player2.getPlayerHighCard(), result, random);
        }

        System.out.println(String.format("Player 1 (%s) wins %d times!",player1.getPlayerName(), player1Wins ));
        System.out.println(String.format("Player 2 (%s) wins %d times!",player2.getPlayerName(), player2Wins ));
        System.out.println(String.format("Total Ties %d times!", totalTies));
        return resultsLines;
    }

}
