/*
Author: Jaime Lopez
Date: 5 Nov 2020
UpD: 5 Nov 2020
Descri: Class of Cards
 */
/*
Notes: 5 Nov 2020: test the current card methods
 */

public class Card implements Comparable<Card> {

    /*
    Fields
     */
    private int cardValue;
    private char cardSuite;

    /*
    Getters, Setters
     */
    public int getCardValue() {
        return this.cardValue;
    }

    public char getCardSuite() {
        return this.cardSuite;
    }

    private void setCardValue(int cardValue){
        this.cardValue =cardValue;
    }
    private void setCardSuite(char cardSuite){
        this.cardSuite = cardSuite;
    }

    /*
    Constructors
     */
    public Card(){}

    public Card(String cardAsString) throws Error {
        setCardSuite(cardAsString.charAt(1));

        //Now try doing the numerical value
       switch(cardAsString.charAt(0)) {
           case 'T':
               setCardValue(10);
               break;
           case 'J':
               setCardValue(11);
               break;
           case 'Q':
               setCardValue(12);
               break;
           case 'K':
               setCardValue(13);
               break;
           case 'A':
               setCardValue(14);
               break;
           default:
               int cardNumericalValue = Character.getNumericValue(cardAsString.charAt(0));
               if (1<cardNumericalValue && cardNumericalValue<10)
                   setCardValue(cardNumericalValue);
               else {
                   System.out.println("Card may not be appropriately labeled");
                   setCardValue(0);
               }
               break;
       }

    }

    /*
    Methods
     */
    @Override
    public int compareTo(Card card) {
        int cardDifference = this.cardValue-card.getCardValue();

        if(cardDifference!=0){
            return cardDifference;
        }
        else
            return 0;
    }

    public boolean compareSuit(Card card){

        //return true if there is no difference
        return Character.valueOf(this.cardSuite).compareTo(Character.valueOf(card.getCardSuite()))==0;

    }

}
