/*
Author: Jaime Lopez
Date: 6 Nov 2020
UpD: 6 Nov 2020
Descri: Enum of Rankings
 */
/*
Notes: 6 Nov 2020: test the current card methods
 */
public enum HandRankings {

    HIGH_CARD, // Highest value card
    ONE_PAIR, // Two cards of the same value
    TWO_PAIRS, // Two different pairs
    THREE_OF_A_KIND, // Three cards all same value
    STRAIGHT, // All cards are consecutive values
    FLUSH, // All cards of the same suit
    FULL_HOUSE, // Three of a kind and a pair
    FOUR_OF_A_KIND, // Four cards of the same value
    STRAIGHT_FLUSH, // All cards are consecutive values of same suit
    ROYAL_FLUSH, // Ten, Jack, Queen, King, Ace, in same suit

}
