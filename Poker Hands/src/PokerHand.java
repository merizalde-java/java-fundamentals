/*
Author: Jaime Lopez
Date: 6 Nov 2020
UpD: 12 Nov 2020
Descri: Class of Cards
 */
/*
Notes: 12 Nov 2020: still need to test this out
 */

import java.util.ArrayList;
import java.util.Arrays;

public class PokerHand implements Comparable<PokerHand> {

    private Card[] cards;
    private Card highCard;
    private Card secondaryHighCard;
    private HandRankings handRank = HandRankings.HIGH_CARD;
    private ArrayList highCardList = new ArrayList();
    private int[] onePairHighCards;

    /*
    Getters Setters
     */

    //getters
    public Card getHighCard(){
        return this.highCard;
    }
    public ArrayList getHighCardList(){ return this.highCardList; }
    public HandRankings getHandRank(){
        return this.handRank;
    }
    public Card[] getPokerHand(){
        return this.cards;
    }
    public String getPokerHandToString(){
        return PokerHandToString(cards);
    }

    //Setters
    private void setHighCard(Card highCard){
        this.highCard = highCard;
    }
    private void setSecondaryHighCard(Card secondaryHighCard) { this.secondaryHighCard = secondaryHighCard;}
    private void setHandRank(HandRankings handRank){
        this.handRank = handRank;
    }

    /*
    Constructors
     */
    public PokerHand(){}
    //Overloaded constructor
    //as Card[]: suppose we already have an array of Cards
    public PokerHand( Card[] cards ){
        //receive an array of cards
        cards = cards;
        setHighCard(FindHighCard(cards));
        onePairHighCards = new int[cards.length - 2];
    }
    //as String[]: going to be most common based on the DataHandler Class
    public PokerHand( String[] cards ){

        //Turn the string array into card array objects
        Card[] cardArray = new Card[cards.length];
        for(int idx=0; idx<cards.length; idx++){ cardArray[idx] = new Card(cards[idx]); }

        //return instantiated array
        this.cards = cardArray;
        setHighCard(FindHighCard(cardArray));
        onePairHighCards = new int[cards.length - 2];
    }

    /*
   Methods
    */

    private Card FindHighCard(Card[] cards){

        //Sort an the array using the implemented method!
        Arrays.sort(cards);
        return cards[cards.length-1];
    }

    //Overloaded: no arg call
    public void FindHandRank(){
        FindHandRank(cards);
    }

    //Overloaded: card[]
    private void FindHandRank(Card[] cards){
        /*
        in order of rank
        ONE_PAIR, TWO_PAIRS, THREE_OF_A_KIND, FULL_HOUSE, FOUR_OF_A_KIND
         */

        //Pre-order the cards array to cut down on work
        Arrays.sort(cards);

        //Cards Comparisons
        int pairStreak = 0, suitStreak = 0, straightStreak = 0;
        boolean haveFlush = false, haveStraightStreak = true;
        for (int idxCard = 0; idxCard < cards.length-1; idxCard++){


            //Same suit check / Flush check
            if( cards[idxCard].compareSuit(cards[idxCard+1]) ){
                //advance the suit streak
                suitStreak++;
                if (suitStreak == 4) {
                    haveFlush = true;
                    RankUpdate(pairStreak, haveFlush, haveStraightStreak, cards[idxCard]);
                }
            }

            //compare current card to the next
            if( haveStraightStreak && cards[idxCard].compareTo(cards[idxCard+1]) == -1 ){
                //Check if you are continuing a straight streak

                //advance the straight streak
                straightStreak++;
                pairStreak = 0;

                if (straightStreak == 4) {
                    RankUpdate(pairStreak, haveFlush, haveStraightStreak, cards[idxCard]);

                }


            } else if( cards[idxCard].compareTo(cards[idxCard+1]) == 0 ){

                //lost the StraightStreak;
                haveStraightStreak = false;

                //if you find a match, you're now on a pair streak
                pairStreak++;

                //Check if this is the penultimate card, so we can still call the update
                if (idxCard == cards.length-2){
                    RankUpdate(pairStreak, haveFlush, haveStraightStreak, cards[idxCard]);
                }

            } else {

                haveStraightStreak = false;

                //any pair streak is over, and consecutive cards don't exist
                RankUpdate(pairStreak, haveFlush, haveStraightStreak, cards[idxCard]);

                //Reset the pair streak
                pairStreak = 0;

                //Check if this is the penultimate card, so we can still call the update
                //to correctly label the last card
                if (idxCard == cards.length-2){
                    RankUpdate(pairStreak, haveFlush, haveStraightStreak, cards[idxCard+1]);
                }
            }
        }
    }

    //Overloaded: int, Card
    private void RankUpdate(int pairStreak, Card card){
        /*
        Key for the evaluation of subhand Rankings
        H = highCard, P = one pair, 2 = two pair, 3 = three of a kind,
         */

        if(pairStreak == 1){
            //you have a pair

            //upgrade the handRank
            switch(this.handRank) {
                //these cases dont' need the full qualification
                //compiler knows what to do with the enum types in
                // a switch statement based on the type declared
                case HIGH_CARD:
                    setHandRank(HandRankings.ONE_PAIR);
                    setHighCard(card);
                    break;
                case ONE_PAIR:
                    setHandRank(HandRankings.TWO_PAIRS);
                    setSecondaryHighCard(card);
                    break;
                case THREE_OF_A_KIND:
                    setHandRank(HandRankings.FULL_HOUSE);
                    break;
                // no default method
            }
        }
        else if(pairStreak == 2){
            //found a triple!

            setHighCard(card);

            switch(handRank) {
                case HIGH_CARD:
                    setHandRank(HandRankings.THREE_OF_A_KIND);
                    break;
                case ONE_PAIR:
                    setHandRank(HandRankings.FULL_HOUSE);
                    break;
                // no default method
            }
        } else if(pairStreak == 3) {
            //found a quadruple

            setHandRank(HandRankings.FOUR_OF_A_KIND);
            setHighCard(card);
        }
    }

    //Overloaded: int and bool, Card
    private void RankUpdate(int pairStreak, boolean haveFlush, Card card){

        RankUpdate(pairStreak, card);

        //now see if you have a flush and your ranking is currently lower, update to flush
        if (haveFlush){


            if (this.handRank.compareTo(HandRankings.STRAIGHT) == 0  && highCard.getCardValue() == 14 ) {
                setHandRank(HandRankings.ROYAL_FLUSH);
            }
            else if( this.handRank.compareTo(HandRankings.STRAIGHT) == 0 ) {
                setHandRank(HandRankings.STRAIGHT_FLUSH);
            }
            else if( this.handRank.compareTo(HandRankings.FLUSH) < 0 )
                setHandRank(HandRankings.FLUSH);
        }
    }

    //Overloaded: int,  bool,  bool, Card
    private void RankUpdate(int pairStreak, boolean haveFlush, boolean haveStraightStreak, Card card){

        if(haveStraightStreak){
            if( this.handRank.compareTo(HandRankings.STRAIGHT) < 0 ) {
                //elevate rank to STRAIGHT
                setHandRank(HandRankings.STRAIGHT);

                RankUpdate(pairStreak, haveFlush, card);
            }
        } else {
            RankUpdate(pairStreak, haveFlush, card);
        }

    }

    public String PokerHandToString(Card[] cards){
        //receive an array of cards
        //return a String
        String[] cardsString= new String[cards.length];

        for(int idxCard = 0; idxCard < cards.length; idxCard++){
            cardsString[idxCard] = String.format("%d%c", cards[idxCard].getCardValue(), cards[idxCard].getCardSuite());
        }

        return String.format("%s %s %s %s %s", cardsString[0], cardsString[1], cardsString[2], cardsString[3], cardsString[4]);
    }

    private void setTertiaryHighCard(Card[] cards){
        if (cards[0].compareTo(cards[1])!=0){
            //First place:
            setHighCard(cards[0]);
        } else if(cards[3].compareTo(cards[4])!=0){
            setHighCard(cards[4]);
        } else
            setHighCard(cards[2]);
    }

    @Override
    public int compareTo(PokerHand pokerHand) {

        if (handRank.compareTo(pokerHand.handRank) != 0) {

            return handRank.compareTo(pokerHand.handRank);

        } else if (handRank == HandRankings.ROYAL_FLUSH) {
            //This is a  comparison that results in a true tie.
            System.out.println("Two Hands are exactly equal Royal Flushes");
            return 0;

        } else if (handRank == HandRankings.THREE_OF_A_KIND || handRank == HandRankings.STRAIGHT
                || handRank == HandRankings.FULL_HOUSE || handRank == HandRankings.FOUR_OF_A_KIND
                || handRank == HandRankings.STRAIGHT_FLUSH) {

            //only the high card matters.
            return this.highCard.compareTo(pokerHand.highCard);

        } else if ( handRank == HandRankings.FLUSH ) {
            //Walk down the row and return
            Arrays.sort(cards);
            Arrays.sort(pokerHand.cards);

            int idxHighCard = cards.length-1;//4
            while( cards[idxHighCard].compareTo(pokerHand.cards[idxHighCard]) == 0 && idxHighCard >= 0){
                idxHighCard = idxHighCard == 0? 0 : idxHighCard--;
                if(idxHighCard == 0)
                    break;
            }
            return cards[idxHighCard].compareTo(pokerHand.cards[idxHighCard]);


        } else if (handRank == HandRankings.TWO_PAIRS){
                if (highCard.compareTo(pokerHand.highCard)!= 0){
                    return highCard.compareTo(pokerHand.highCard);
                } else if (secondaryHighCard.compareTo(pokerHand.secondaryHighCard)!=0){
                    return secondaryHighCard.compareTo(pokerHand.secondaryHighCard);
                } else {
                    setTertiaryHighCard(pokerHand.cards);
                    setTertiaryHighCard(this.cards);
                    return highCard.compareTo(pokerHand.highCard);
                }
        } else if (handRank == HandRankings.ONE_PAIR){
            if (highCard.compareTo(pokerHand.highCard) != 0){
                return highCard.compareTo(pokerHand.highCard);
            } else {

                //diag
                //Find some cards that are not the high card.
                int idxCards = 0;
                for (int idx2 = 0; idx2 < cards.length; idx2++){
                    if (cards[idx2].compareTo(highCard)!=0 && idxCards < cards.length - 2){
                        onePairHighCards[idxCards] = cards[idx2].getCardValue();
                        //diag
                        System.out.println(onePairHighCards[idxCards]);
                        idxCards++;
                    }
                }

                int idxCards2 = 0;
                for (int idx2 = 0; idx2 < cards.length; idx2++){
                    if (pokerHand.cards[idx2].compareTo(pokerHand.highCard)!=0 && idxCards2 < cards.length - 2){

                        pokerHand.onePairHighCards[idxCards2] = pokerHand.cards[idx2].getCardValue();
                        //diag
                        System.out.println(pokerHand.onePairHighCards[idxCards2]);
                        idxCards2++;
                    }
                }

                //now you can do the comparison
                int idxHighCard = onePairHighCards.length-1;//4
                while( onePairHighCards[idxHighCard] == pokerHand.onePairHighCards[idxHighCard]  && idxHighCard >= 0){
                    idxHighCard = idxHighCard == 0? 0 : idxHighCard--;
                    if (idxHighCard == 0);
                        break;
                }

                return Integer.compare(onePairHighCards[idxHighCard], pokerHand.onePairHighCards[idxHighCard]);
            }
        } else if (handRank == HandRankings.HIGH_CARD){
            //Walk through the ordered cars one-by-one until you find a mismatch.
            //It is possible to have the entire hands equal to each other resulting in a tie.
            Arrays.sort(cards);
            Arrays.sort(pokerHand.cards);

            int idxHighCard = cards.length-1;//4
            while( cards[idxHighCard].compareTo(pokerHand.cards[idxHighCard]) == 0 && idxHighCard >= 0){
                idxHighCard = idxHighCard == 0? 0 : idxHighCard--;
                if (idxHighCard == 0);
                break;
            }
            return cards[idxHighCard].compareTo(pokerHand.cards[idxHighCard]);
        }

        return 0;
    }
}


