# Author: Jaime Lopez-Merizalde
# Date: 5 11 2020
# Last Update: 5 11 2020
# Title: script.jl
# Descripition:
#  test some Java things
# this code written in Julia 0.5.4
# REPO:
#NOTES:written for Julia 0.5.4
#

##-----Libraries
# include("$(pwd())/library/engines/names_splitting_engine.jl")
# include("$(pwd())/library/engines/subStringToString.jl")
# include("$(pwd())/library/engines/addStudentGradesEngine.jl")

using CSV

##DATA
##-----DATA
#-----data file names
#

#load
data_raw_FN_pre = "data/pokerHands.txt"
data_raw_FN = "$(pwd())/../$data_raw_FN_pre"


## save
# save_tips_report_FN = "tips_report.txt"
# save_tips_report = "$(pwd())/$save_tips_report_FN"
#
# save_tips_report_formatted_FN = "tips_report_formatted.txt"
# save_tips_report_formatted = "$(pwd())/$save_tips_report_formatted_FN"

#-----data loading
data = readdlm(data_raw_FN,',')
