package com.pluralsight.letsgetlogical;

public class Main {

    public static void main(String[] args) {
        int students = 150;
        int rooms = 0;

        /*
        This won't work becase we still evaluate the right side of the
        condition.

         */

        //if (rooms != 0 & students / rooms > 30)

        if (rooms != 0 && students / rooms > 30)
            System.out.println("Crowded");

        System.out.println();
        System.out.println("*** end of program ***");

        /*
        block statements
         */

        /*
        as written only thing associated with the if is the statement right below!
        look futher down
         */
        int v1 = 10, v2 = 4;

        final int diff;

        if (v1 > v2) { // need this curly here
            diff = v1 - v2;
            //+ here will string add varible diff
            System.out.println("v1 bigger than v2, diff = " + diff);
        }
        else { //also need this curly here
            diff = v2 - v1;
            System.out.println("v1 not bigger v2, diff = " + diff);
        }

        /*
        scope
         */

        //variables allowed inside a block
        double students2 = 30.0d, rooms2 = 4.0d;
        if (rooms > 0.0d) {
            System.out.println(students);
            System.out.println(rooms);

            //this variable is limited to the block
            double avg = students / rooms;

            //can declare before the if statement
            //or can decide to print this variable within the block
        }

    }
}
