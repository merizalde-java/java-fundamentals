public class Main {

    public static void main(String[] args) {
	// write your code here

        /*
        arrays
         */

        float[] theVals = new float[3];
        theVals[0] = 10.0f;
        theVals[1] = 20.0f;
        theVals[2] = 15.0f;

        //can also do shorthand
        float[] theVals2 = {10.0f, 20.f, 15.0f};

        float sum = 0.0f;

        for(int index = 0; index < theVals.length; index++)
            sum += theVals[index];
        System.out.println(sum);



    }
}
