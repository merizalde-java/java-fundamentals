public class Main {

    //Specific entry point style
    public static void main(String[] args) {

        //to run this: run main or ^R or green arrow
        /*
        This code is for the first app
         */

        System.out.println("First line from app"); //line comment
        System.out.println("Second line from app");
        System.out.println("Third line from app");

    }
}
