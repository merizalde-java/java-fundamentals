package com.pluralsight.letsgetlogical;

public class Main {

    public static void main(String[] args) {
	// write your code here

        /*
        while loop
         */
//        int someValue = 5;
//        int factorial = 1;
//
//        while(someValue >1) {
//            factorial *= someValue;
//            someValue --;
//        }
//        System.out.println(factorial);

        /*
        Do-while
         */

//        int iVal = 80;
//        do {
//            //print is multiple vals on same line
//            //vs println
//            System.out.print(iVal);
//            System.out.print(" * 2 = ");
//            iVal *=2;
//            System.out.println(iVal);
//
//        //this code will run at least once, because while is at the end
//        } while(iVal < 25);

        /*
        for-loop
         */
        //compare
        { //brackets here used to limit the scope
            int i = 1;
            while (i < 100) {
                System.out.println(i);
                i *= 2;
            }
        }

        //VS

        for (int i = 1; i < 100; i*=2)
            System.out.println(i);

    }
}
