package com.pluralsight.organized;

public class Main {

    public static void main(String[] args) {
	System.out.println("we got organized!");

//	int myVar; // uni
//        myVar = 50;
//
//    System.out.println(myVar);
//
//    int anotherVar = 100;
//
//    System.out.println(anotherVar);
//
//    myVar = anotherVar;
//
//    System.out.println(myVar);
//
//    //value of max students cannot change once it is set
//    final int maxStudents = 25;
//
//    // value of some variable cannot change but is not initialized
//    final int someVariable;
//
//    int someOtherVariable = 100;
//
//    someVariable = someOtherVariable;
//
//    int valA = 21;
//    int valB = 6;
//    int valC = 3;
//    int valD = 1;

//    /*
//    Type conversions
//     */
//        float floatVal = 1.0f;
//        double doubleVal = 4.0d;
//        byte byteVal = 7;
//        short shortVal = 7;
//        long longVal = 5;
//
//        //short larger than a byte, so the widening is no problem
//        short result1 = byteVal;
//
//        /*
//        //it won't like longval because it is a narrowing conversion
//        the compiler does not auto matically do a narrowing conversion.
//        we can still do, but need it explicitly CAST
//        */
//        //short result1 = longVal;
//
//        /*
//        you can do it like this instead
//         */
//        short result2 = (short) longVal;
//
//        /*
//        This won't work because this operation results in a wider type (long),
//         but is attempting to assing to a short
//         */
//        //NG short result3 = byteVal - longVal;
//        //SG >>
//        short result4 = (short) (byteVal - longVal);
//
//        /*
//        something similar happens with long and float
//         */
//        //NG
//        // long result5 = longVal - floatVal;
//        //SG
//        long result6 = (long) (longVal - floatVal);
//
//        /*
//        it's better to just make it a float, because the above calculation will
//        lose accuracy since longs can't store decimals
//         */
//        float result7 = longVal - floatVal;

//        /*
//        conditional Assignments
//         */
//        int value1 = 7;
//        int value2 = 5;
//
//        int maxValue = value1 > value2 ? value1 : value2;
//
//        System.out.println(maxValue);

//        /*
//        if-else
//         */
//
//        int value1 = 10;
//        int value2 = 4;
//
//        if (value1 > value2)
//            System.out.println("value 1 is bigger");
//        else
//            System.out.println("value 1 is not bigger");

        /*
        Chaining if-else together
         */

       int value1 = 10;
       int value2 = 40;

       if (value1 > value2)
           System.out.println("value 1 is bigger");
       else if (value1 < value2)
           System.out.println("value 2 is bigger");
       else
           System.out.println("value 1 and value 2 are equal");



        System.out.println("SUCCESS");

    }
}
