package com.pluralsight.calcengine;

public class Divider extends CalculateBase{

    /*
    Phase 20: abstract and constructor interface
    -see adder for more notes on phase 20
     */

    public Divider(){}

    public Divider(double leftVal, double rightVal){
        super(leftVal, rightVal);
    }

    @Override
    public void calculate() {
        //conditional assignment
        double value = getRightVal() != 0 ? getLeftVal() / getRightVal() : 0;
        setResult(value);
    }
}
