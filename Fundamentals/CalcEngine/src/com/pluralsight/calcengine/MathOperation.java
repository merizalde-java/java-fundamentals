package com.pluralsight.calcengine;

public enum MathOperation {
    /*
    We will use this enum type to list out the math operations
    We will use these ops later on to get called on by our class.
     */

    ADD,
    SUBTRACT,
    MULTIPLY,
    DIVIDE

}
