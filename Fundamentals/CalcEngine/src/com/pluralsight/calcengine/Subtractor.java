package com.pluralsight.calcengine;

public class Subtractor extends CalculateBase{


    /*
    Phase 20: abstract and constructor interface
    -see adder for more notes on phase 20
     */
    public Subtractor(){}

    public Subtractor(double leftVal, double rightVal){
        super(leftVal, rightVal);
    }

    @Override
    public void calculate() {
        double value = getLeftVal()-getRightVal();
        setResult(value);
    }
}
