package com.pluralsight.calcengine;

public class MathEquation {
    /*
    Phase 13: make the class
     */

    //Fields
    private double leftVal;
    private double rightVal;
    private char opCode;
    private double result;

    /*
    Phase 17: static enhancements
     */
    /*
    //want to have the average calculated for
    // each time the MathEquation class is called and
    //instantiates an object of type MathEquation
    //we strategize the call of where this occurs, then
     */

    //first track number of times called

    private static int numberOfCalculations;
    private static double sumOfResults;


    /*
    Constructors
     */
    //0-th constructor
    public MathEquation(){

    }

    //opCode Only
    public MathEquation(char opCode){
        this.opCode = opCode;
    }

    //and op code left and Right val accepted
    public MathEquation(char opCode, double leftVal, double rightVal) {

        this(opCode);//chain to prev. construct.

        this.leftVal = leftVal;
        this.rightVal = rightVal;
    }

    //Just opCode

    /*
    Setters
     */
    public void setLeftVal(double leftVal) {
        this.leftVal = leftVal;
    }

    public void setRightVal(double rightVal) {
        this.rightVal = rightVal;
    }

    public void setOpCode(char opCode) {
        this.opCode = opCode;
    }

    /*
    Getters
     */

    public static double getAverageResult(){
        return sumOfResults/numberOfCalculations;
    }

    public double getResult() {
        return this.result;
    }

    //Some Methods
    public void execute() {
        //run through switch statement
        switch (opCode) {
            case 'a':
                result = leftVal + rightVal;
                break;
            case 's':
                result = leftVal - rightVal;
                break;
            case 'm':
                result = leftVal * rightVal;
                break;
            case 'd':
                //conditional assignment
                result = rightVal != 0 ? leftVal / rightVal : 0;
                break;
            default:
                System.out.println("Invalid opCode: " + opCode);
                result = 0.0d;
                break;
        }

        /*
        Phase 17:
         */

        numberOfCalculations++;
        sumOfResults += result;
    }

    /*
    Phase 18: overloading methods
     */
    public void execute(double leftVal, double rightVal){

        this.leftVal = leftVal;
        this.rightVal = rightVal;

        //now call the original execute method
        execute();

    }
    //overloaded to handle specifically ints
    public void execute(int leftVal, int rightVal){

        //this.leftVal and rightVal are still doubles so the operation
        //still does a widening conversion even accidentally.
        //instead, we can cast the result as an int as below
        this.leftVal = leftVal;
        this.rightVal = rightVal;

        //now call the original execute method
        execute();

        result = (int) result;
    }

    /*
    Phase 19: class inheritance
     */

}
