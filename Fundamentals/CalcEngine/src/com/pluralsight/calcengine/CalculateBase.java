package com.pluralsight.calcengine;

public abstract class CalculateBase {

    //Fields
    private double leftVal;
    private double rightVal;
    private double result;

    /*
    Phase 20: abstract and constructor interface
    -let's add a constructor here for the calcbase
    -although we add these, we need the classes that
    -inherit from this to also implement these constructors
    */

    //default constructor
    public CalculateBase(){}

    //constructor that accepts left and right vals
    public CalculateBase(double leftVal, double rightVal){
        this.leftVal = leftVal;
        this.rightVal = rightVal;
    }


    //getters setters

    public double getLeftVal() {
        return leftVal;
    }
    public double getRightVal() {
        return rightVal;
    }
    public double getResult() {
        return result;
    }

    public void setRightVal(double rightVal) {
        this.rightVal = rightVal;
    }
    public void setLeftVal(double leftVal) {
        this.leftVal = leftVal;
    }
    public void setResult(double result) {
        this.result = result;
    }

    /*
    // no some methods: though we want the work to be done
    // elsewhere entirely!
    */
    /*
    Phase 20: abstract and constructor interface
    We expect Calculate BASe class to be extended and
    for this method to be overridden.
    We need to declare this method as abstract
    */

    public abstract void calculate();
}
