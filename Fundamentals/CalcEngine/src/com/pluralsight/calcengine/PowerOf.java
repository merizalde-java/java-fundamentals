package com.pluralsight.calcengine;

public class PowerOf implements MathProcessing{

    /*
    The power of class now supports all three methods in the implement
    While the adder class only supports the two original methods
     */

    private String formattedOutput;

    @Override
    public String getKeyWord() {
        return "power";
    }

    @Override
    public double doCalculation(double leftVal, double rightVal) {
        //We can just use JAVA's Math class
        double product =  Math.pow(leftVal, rightVal);

        //prepares the formattedOutput
        formattedOutput = leftVal + " to the power of " + rightVal + " is " + product;

        return product;
    }

    @Override
    public String getFormattedOutput() {
        return formattedOutput;
    }
}
