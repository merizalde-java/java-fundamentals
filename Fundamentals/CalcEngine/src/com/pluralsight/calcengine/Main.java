package com.pluralsight.calcengine;

/*
Phase 8: interactive execution
 */
//imported this for a method below
import java.time.LocalDate;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        /*
        phase 1  initial of calcEngine demo
        this is a simple calculator
         */

//        double value1 = 100.0d;
//        double value2 = 50.0d;
//        double result = 0;
//        char opCode = 'd';
//
//        if (opCode == 'a')
//            result = value1 + value2;
//        else if (opCode == 's')
//            result = value1 - value2;
//        else if (opCode == 'm')
//            result = value1*value2;
//        else if (opCode == 'd')
//            result = value1/value2;
//        else
//            result = 0.0d;
//
//        System.out.println(result);

        /*
        phase 2 of calc engine demo
         */
//
//        double value1 = 100.0d;
//        double value2 = 0.0d;
//        double result = 0.0d;
//        char opCode = 'd';
//
//        if (opCode == 'a')
//            result = value1 + value2;
//        else if (opCode == 's')
//            result = value1 - value2;
//        else if (opCode == 'm')
//            result = value1*value2;
//        else if (opCode == 'd') {
//            //need this { or the else will be associated with the wrong elseif
//            if (value2 != 0)
//                result = value1 / value2;
//        }
//        else {
//            //need these block brackets or it won't run the subsequent line
//            System.out.println("Invalid opCode: " + opCode);
//            result = 0.0d;
//        }
//
//        System.out.println(result);

        /*
        phase 3: using switch statements
         */
//        double value1 = 100.0d, value2 = 0.0d, result = 0.0d;
//        char opCode = 'd';
//
//        switch(opCode) {
//            case 'a':
//                result = value1 + value2;
//                break;
//            case 's':
//                result = value1 - value2;
//                break;
//            case 'm':
//                result = value1*value2;
//                break;
//            case 'd':
//                //conditional assignment
//                result = value2!=0 ? value1/value2 : 0;
//                break;
//            default:
//                System.out.println("Invalid opCode: "+opCode);
//                result = 0.0d;
//                break;
//
//        }

        /*
        Phase 4: introducing arrays to calc engine and with for-each for printing
         */

//        /*
//        these are parallel arrays: elements meant to be used
//        with corresponding elements in other arrays
//         */
//        double[] leftVals = {100.0d, 25.0d, 225.0d, 11.0d};
//        double[] rightVals = {50.0d, 92.0d, 17.0d, 3.0d};
//        char[] opCodes = {'d', 'a', 's', 'm'};
//        //this one we specify the size
//        double[] results = new double[opCodes.length];
//
//        for (int idx = 0; idx < opCodes.length; idx++) {
//            switch (opCodes[idx]) {
//                case 'a':
//                    results[idx] = leftVals[idx] + rightVals[idx];
//                    break;
//                case 's':
//                    results[idx] = leftVals[idx] - rightVals[idx];
//                    break;
//                case 'm':
//                    results[idx] = leftVals[idx] * rightVals[idx];
//                    break;
//                case 'd':
//                    //conditional assignment
//                    results[idx] = rightVals[idx] != 0 ? leftVals[idx] / rightVals[idx] : 0;
//                    break;
//                default:
//                    System.out.println("Invalid opCode: " + opCodes[idx]);
//                    results[idx] = 0.0d;
//                    break;
//            }
//        }
//
//        for (double idxCurrentResult: results)
//            System.out.println(idxCurrentResult);

        /*
        Phase 5: using methods on calc engine
         */

//        /*
//        these are parallel arrays: elements meant to be used
//        with corresponding elements in other arrays
//         */
//        double[] leftVals = {100.0d, 25.0d, 225.0d, 11.0d};
//        double[] rightVals = {50.0d, 92.0d, 17.0d, 3.0d};
//        char[] opCodes = {'d', 'a', 's', 'm'};
//        //this one we specify the size
//        double[] results = new double[opCodes.length];
//
//        for (int idx = 0; idx < opCodes.length; idx++) {
//            results[idx] = executeOperation(opCodes[idx], leftVals[idx], rightVals[idx]);
//        }
//
//        for (double idxCurrentResult: results)
//            System.out.println(idxCurrentResult);

        /*
        Phase 6: command line support
         */
//        double[] leftVals = {100.0d, 25.0d, 225.0d, 11.0d};
//        double[] rightVals = {50.0d, 92.0d, 17.0d, 3.0d};
//        char[] opCodes = {'d', 'a', 's', 'm'};
//        //this one we specify the size
//        double[] results = new double[opCodes.length];
//
//        //did we recieve command line args?
//        if (args.length == 0) {
//            //No we did not, just run the code as usual
//            for (int idx = 0; idx < opCodes.length; idx ++) {
//                results[idx] = executeOperation(opCodes[idx], leftVals[idx], rightVals[idx]);
//            }
//            for (double currentResult : results)
//                //for each command line arge
//                System.out.println(currentResult);
//
//        } else if(args.length == 3)
//            // if there are 3 args which correspond to the opCodes, and left and right values
//            handleCommandLine(args);
//        else
//            System.out.println("Must provide operation code and 2 numerical values");
//

        /*
        Phase 7: adding string support
         */

//        double[] leftVals = {100.0d, 25.0d, 225.0d, 11.0d};
//        double[] rightVals = {50.0d, 92.0d, 17.0d, 3.0d};
//        char[] opCodes = {'d', 'a', 's', 'm'};
//        double[] results = new double[opCodes.length];
//
//        //check arg size for multi-mode input
//        if (args.length == 0) {
//            //if no user input from command line, just go through the
//            //available opcodes in this main
//            for (int idx = 0; idx < opCodes.length; idx++) {
//                results[idx] = executeOperation(opCodes[idx], leftVals[idx], rightVals[idx]);
//            }
//        } else if (args.length ==3)
//            //otherwise use the user in-put codes
//            handleCommandLine(args);
//        else
//            System.out.println("Must provide operation code and 2 numerical values");

        /*
        Phase 8: interactive execution
         */

//        double[] leftVals = {100.0d, 25.0d, 225.0d, 11.0d};
//        double[] rightVals = {50.0d, 92.0d, 17.0d, 3.0d};
//        char[] opCodes = {'d', 'a', 's', 'm'};
//        double[] results = new double[opCodes.length];
//
//        //check arg size for multi-mode input
//        if (args.length == 0) {
//            //if no user input from command line, just go through the
//            //available opcodes in this main
//            for (int idx = 0; idx < opCodes.length; idx++) {
//                results[idx] = executeOperation(opCodes[idx], leftVals[idx], rightVals[idx]);
//            }
//        } else if (args.length == 1 && args[0].equals("interactive")) {
//            //check if the entered interactive mode
//            executeInteractively();
//        } else if (args.length ==3)
//            //otherwise use the user in-put codes
//            handleCommandLine(args);
//        else
//            System.out.println("Must provide operation code and 2 numerical values");

        /*
        Phase 9: building string output
         */

//        //while the user may enter add six nine
//        //we want to display the result of this in a familiar mathematical statement
//        //6 + 9 = result
//
//        double[] leftVals = {100.0d, 25.0d, 225.0d, 11.0d};
//        double[] rightVals = {50.0d, 92.0d, 17.0d, 3.0d};
//        char[] opCodes = {'d', 'a', 's', 'm'};
//        double[] results = new double[opCodes.length];
//
//        //check arg size for multi-mode input
//        if (args.length == 0) {
//            //if no user input from command line, just go through the
//            //available opcodes in this main
//            for (int idx = 0; idx < opCodes.length; idx++) {
//                results[idx] = executeOperation(opCodes[idx], leftVals[idx], rightVals[idx]);
//            }
//        } else if (args.length == 1 && args[0].equals("interactive")) {
//            //check if the entered interactive mode
//            executeInteractively();
//        } else if (args.length ==3)
//            //otherwise use the user in-put codes
//            handleCommandLine(args);
//        else
//            System.out.println("Must provide operation code and 2 numerical values");


        /*
        Phase 10: Adding String Formatting
         */

//        double[] leftVals = {100.0d, 25.0d, 225.0d, 11.0d};
//        double[] rightVals = {50.0d, 92.0d, 17.0d, 3.0d};
//        char[] opCodes = {'d', 'a', 's', 'm'};
//        double[] results = new double[opCodes.length];
//
//        //check arg size for multi-mode input
//        if (args.length == 0) {
//            //if no user input from command line, just go through the
//            //available opcodes in this main
//            for (int idx = 0; idx < opCodes.length; idx++) {
//                results[idx] = executeOperation(opCodes[idx], leftVals[idx], rightVals[idx]);
//            }
//        } else if (args.length == 1 && args[0].equals("interactive")) {
//            //check if the entered interactive mode
//            executeInteractively();
//        } else if (args.length ==3)
//            //otherwise use the user in-put codes
//            handleCommandLine(args);
//        else
//            System.out.println("Must provide operation code and 2 numerical values");

//        /*
//        Phase 11: adding date time calculations
//         */
//        double[] leftVals = {100.0d, 25.0d, 225.0d, 11.0d};
//        double[] rightVals = {50.0d, 92.0d, 17.0d, 3.0d};
//        char[] opCodes = {'d', 'a', 's', 'm'};
//        double[] results = new double[opCodes.length];
//
//        /*
//        Phase 14: Using MathEquation Class
//         */
//
//        //declare array of references of type math equation.
//        MathEquation[] equations = new MathEquation[4];
//
//        //each element must create its instance
//        equations[0] = new MathEquation();
//        /*
//        The math equation class does not have a constructor and each value
//        is not pre-populated.
//        We must define it here for each class instance
//        */
//        equations[0].leftVal = 100.0d;
//        equations[0].rightVal = 50.0d;
//        equations[0].opCode = 'd';
//
//        /*
//        alternatively we can add a new method to the math equation class
//        that encapsulates all of this explicit work, removing it
//        from the main method...
//         */
//        equations[0] = create(100.0d, 50.0d, 'd');
//        equations[1] = create(25.0d, 92.0d, 'a');
//        equations[2] = create(225.0d, 17.0d, 's');
//        equations[3] = create(11.0d, 3.0d, 'm');
//
//
//        //check arg size for multi-mode input
//        if (args.length == 0) {
//            //if no user input from command line, just go through the
//            //available opcodes in this main
//            for (int idx = 0; idx < opCodes.length; idx++) {
//                results[idx] = executeOperation(opCodes[idx], leftVals[idx], rightVals[idx]);
//            }
//        } else if (args.length == 1 && args[0].equals("interactive")) {
//            //check if the entered interactive mode
//            executeInteractively();
//        } else if (args.length ==3)
//            //otherwise use the user in-put codes
//            handleCommandLine(args);
//        else
//            System.out.println("Must provide operation code and 2 numerical values");

        /*
        Phase 15: Using Classes with CalcE
         */
        //performCalculations();

        /*
        Phase 19: Class Inheritance
         */
        //instantiate new object of class Divider
        //Divider divider = new Divider();
        /*
         pass the reference to this class to the doCalculation method,
         the doCalculation method, accepts this as CalculateBase reference
         despite this, the divider reference which inherits this class,
         overrides with its execute() method, which JAVA will apply
         because references use the instantiated object type
         not the reference type when it comes to methods.
         */
        //doCalculation(divider, 100.0d, 50.0d);

        //Adder adder = new Adder();
        //doCalculation(adder, 25.0d, 92.0d);

        /*
        Phase 20: Using Constructor Inheritance
         */
          //performMoreCalculations();

        /*
        Phase 22: Processing Interactive Input in CalcEngine
         */
            //executeInteractively();

        /*
        Phase 23: Interfaces
         */
        dynamicInteractivity();


    }//END MAIN


    /*
    METHODS METHODS METHODS
     */

    /*
      Phase 23: Interfaces
      Phase 24: New Implementation of Interface
     */
    private static void dynamicInteractivity(){
        /*
        Instantiate Dynamic Helper class,
        which uses a MathProcessing interface array,
         and the classes that implement it can be used in it's initialization
         */
        /*
        this is basically a list of DynamicHelpers that we are willing to work with
        because of the contract
         */
        DynamicHelper helper = new DynamicHelper(new MathProcessing[]{
                new Adder(),
                new PowerOf(),
                new LogOf()
        });

        System.out.println("Dynamic Interactivity");
        System.out.println("Enter an operation and two numbers");
        Scanner scanner = new Scanner(System.in);
        String userInput = scanner.nextLine();

        helper.process(userInput);
    }

    /*
    Phase 20: Using Constructor Inheritance
    */
    private static void performMoreCalculations() {
        //can create a calculate base array using instances of
        //calculation classes which inherit from the CalculateBase!
        CalculateBase[] calculations = {
                new Divider(100.0d, 50.0d),
                new Adder(25.0d, 17.0d),
                new Subtractor(225.0d, 17.0d),
                new Multiplier(11.0d, 3.0d)
        };

        //Blank Line for Fun
        System.out.println();
        System.out.println("Array Calculations ");

        for (CalculateBase calculation : calculations) {
            calculation.calculate();
            System.out.println(calculation.getResult());
        }
    }


    /*
    PHASE 21: WORKING W ENUMS
     */
    /*
    were using a method called create calculation of type calculateBase which uses enums
    to define the operation we will be performing on certain values
     */
    private static CalculateBase createCalculation(MathOperation operation, double leftVal, double rightVal) {
        //start it as null b/c soon we will have a reference to whatever class inst. we create
        CalculateBase calculation = null;

        switch (operation) {
            case ADD:
                calculation = new Adder(leftVal, rightVal);
                break;
            case SUBTRACT:
                calculation = new Subtractor(leftVal, rightVal);
                break;
            case MULTIPLY:
                calculation = new Multiplier(leftVal, rightVal);
                break;
            case DIVIDE:
                calculation = new Divider(leftVal, rightVal);
                break;
        }

        return calculation;
    }

    /*
    Phase 15: actually executes the guts we intended to have in previous iterations
     */
    private static void performCalculations() {

        //declare array of references of type math equation.
        MathEquation[] equations = new MathEquation[4];

        //each element must create its instance
        equations[0] = new MathEquation();
        /*
        The math equation class does not have a constructor and each value
        is not pre-populated.
        We must define it here for each class instance
        */
        //Once I provide getters/setters to the class, we can no longer directly access these values
//        equations[0].leftVal = 100.0d;
//        equations[0].rightVal = 50.0d;
//        equations[0].opCode = 'd';

        /*
        alternatively we can add a new method to the math equation class
        that encapsulates all of this explicit work, removing it
        from the main method...
         */
        /*
        Phase 16: we are doing this work now in the constructor!
         */
        equations[0] = new MathEquation('d', 100.0d, 50.0d);
        equations[1] = new MathEquation('a', 25.0d, 92.0d);
        equations[2] = new MathEquation('s', 225.0d, 17.0d);
        equations[3] = new MathEquation('m', 11.0d, 3.0d);

//        equations[0] = create(100.0d, 50.0d, 'd');
//        equations[1] = create(25.0d, 92.0d, 'a');
//        equations[2] = create(225.0d, 17.0d, 's');
//        equations[3] = create(11.0d, 3.0d, 'm');


        for (MathEquation equation : equations) {
            //now we want to execute the work on each equation as it loops
            //through the equations array.
            equation.execute();
//            System.out.println("result = " + equation.result);
            System.out.println("result = " + equation.getResult());
        }

        System.out.println("Mean Result = " + MathEquation.getAverageResult());

        /*
        Phase 18: using method overloads
         */
        System.out.println();
        System.out.println("Using Method overloads");
        System.out.println();

        //mathequation class will know what to do with this
        MathEquation equationOverload = new MathEquation('d');

        //now try doing something with two values
        double leftDouble = 9.0d;
        double rightDouble = 4.0d;

        equationOverload.execute(leftDouble, rightDouble);
        System.out.println("Overload result with doubles: " + equationOverload.getResult());

        int leftInt = 9;
        int rightInt = 4;
        equationOverload.execute(leftInt, rightInt);
        System.out.println("Overload result with ints: " + equationOverload.getResult());

        /*
        Phase 19: Class Inheritance
         */


    }

    /*
    Phase 16: with new constructors in place , we can remove this method below
    b/c the work can be done using the constructors.
     */
//    private static MathEquation create(double leftVal, double rightVal, char opCode) {
//        //create new math equation instance
//        //this is where the work for any new object of the MathEquation class will receive
//        // its initial values
//        MathEquation equation = new MathEquation();
//
//        /*
//        Method was made private >>>
//         */
////        equation.leftVal = leftVal;
////        equation.rightVal = rightVal;
////        equation.opCode = opCode;
//
//        //updated using getters and setters
//        equation.setLeftVal(leftVal);
//        equation.setRightVal(rightVal);
//        equation.setOpCode(opCode);
//
//        return equation;
//    }


    /*
    Phase 5: using methods
     */
    /*
    Phase 17: implement MathEquations static members
     */
    static double executeOperation(char opCode, double leftVal,double rightVal) {
        //do calculation, return the result

        //declare result;
        double result;

        //run through switch statement
        switch (opCode) {
            case 'a':
                result = leftVal + rightVal;
                break;
            case 's':
                result = leftVal - rightVal;
                break;
            case 'm':
                result = leftVal * rightVal;
                break;
            case 'd':
                //conditional assignment
                result = rightVal != 0 ? leftVal / rightVal : 0;
                break;
            default:
                System.out.println("Invalid opCode: " + opCode);
                result = 0.0d;
                break;
        }
        return result;
    }


    /*
    Phase 6: command line support
     */
    private static void handleCommandLine(String[] args) {
        //args come in as strings
        //we will use this as the interpreter between the cmd line args
        // and the executeOperation method

        //need to convert each arg into appropriate type for the execute method

        //take the first arg
        //charAt take string of a character and translates to the char representation we need
        char opCode = args[0].charAt(0);

        //this uses a method that parses a char, string into a double:
        double leftVal = Double.parseDouble(args[1]);

        //same thing for right val
        double rightVal = Double.parseDouble(args[2]);

        double result = executeOperation(opCode, leftVal, rightVal);

        System.out.println(result);

    }

    /*
    Phase 7: string support
     */
    //need to translate the user input into an opcode
    static char opCodeFromString(String operationName) {
        /*
        //how will we update the words based on what was sent?
        //grab first letter from word that was sent
        charAt takes character at the given index with zero-based
        indexing
        */
        char opCode = operationName.charAt(0);

        return opCode;
    }

    //now translation work for numeric words
    /*
    STATIC, again
     */
    /*
    silenced for phase 12
     */
//    static double valueFromWord(String word) {
//        /*
//        these words are for 10 digits
//        This is an array with all the matching
//        words for the numbers available
//        */
//
//        String[] numberWords = {
//                "zero", "one", "two", "three", "four",
//                "five", "six", "seven", "eight", "nine"
//        };
//
//        //now the numeric value
//        double value = 0d;
//        for (int index = 0; index < numberWords.length; index++) {
//            if(word.equals(numberWords[index])) {
//                value = index;
//                break; //break out of the loop
//            }
//        }
//        return value;
//    }

    /*
    Phase 8: interactive execution
    Phase 22: Processing Interactive Input in CalcEngine
     */

    static void executeInteractively(){
        //Start interaction w user by asking for input
        System.out.println("Enter operation followed by two numbers");

        //To get input:
        Scanner scanner = new Scanner(System.in);
        //scanner will accept all user input as a string until
        // user hits enter (that's what nextLine is for)
        String userInput = scanner.nextLine();

        /*
        //now need to split user input into individual parts
        //this split is based on spaces, so enter that character
        assign that to an array called parts
        */
        String[] parts = userInput.split(" ");

        //once you have the input, call a new method that does the operation work
        performOperation2(parts);
    }

//    private static void performMethod(String[] parts) {
//
//        //opcode
//        char opCode = opCodeFromString(parts[0]);
//
//        //leftVal
//        double leftVal = valueFromWord(parts[1]);
//
//        //rightVal
//        double rightVal = valueFromWord(parts[2]);
//
//        //do the operation
//        double result = executeOperation(opCode, leftVal, rightVal);
//
//        /*
//        Phase 9: building string output
//         */
//        //don't just print the result, but use the method
//        // created to give some context to the problem
//        //old: System.out.println(result);
//        displayResult(opCode, leftVal, rightVal, result);
//
//    }

    /*
    Phase 9: building string output
     */
    private static char symbolFromOpCode(char opCode) {

        //parallel arrays
        char[] opCodes = {'a', 's', 'm', 'd'};
        char[] symbols = {'+','-', '*', '/'};

        char symbol = ' '; //initialize symbol
        for (int idxCode = 0; idxCode < opCodes.length; idxCode++) {
            if (opCode == opCodes[idxCode]) {
                symbol = symbols[idxCode];
                //remember chars can behave w == no problem
                //strings cannot do that!
                break;
            }
        }

        return symbol;
    }

//    private static void displayResult(char opCode, double leftVal, double rightVal, double result){
//        char symbol = symbolFromOpCode(opCode);
//
//        StringBuilder builder = new StringBuilder(20);
//
//        /*
//        this right here will place it in teh right context
//        we are building a string but it doesn't matter because
//        string builder class will convert types automatically
//        */
//
//        builder.append(leftVal);
//        builder.append(" ");
//        builder.append(symbol);
//        builder.append(" ");
//        builder.append(rightVal);
//        builder.append(" = ");
//        builder.append(result);
//
//        String output = builder.toString();
//        System.out.println(output);
//    }

    /*
    Phase 10: Adding String Formatting
     */

    //modifies Display Result Method from Phase 9

    private static void displayResult(char opCode, double leftVal, double rightVal, double result){
        char symbol = symbolFromOpCode(opCode);
        /*
        //String Format makes the work of the previous version of displayResult
        much cleaner and easier
         */

        //String output = String.format("%f %c %f = %f",leftVal, symbol, rightVal, result);
        //With limited float precision
        String output = String.format("%.3f %c %.3f = %.3f",leftVal, symbol, rightVal, result);

        System.out.println(output);
    }

    /*
    Phase 11: adding date time math
     */
    private static void performOperation(String[] parts) {

        //opcode
        char opCode = opCodeFromString(parts[0]);

        if (opCode == 'w') {
            handleWhen(parts);
        }
        else {

            //leftVal
            double leftVal = valueFromWord(parts[1]);

            //rightVal
            double rightVal = valueFromWord(parts[2]);

            //do the operation
            double result = executeOperation(opCode, leftVal, rightVal);

            displayResult(opCode, leftVal, rightVal, result);
        }

    }

    /*
    Phase 22: Processing Interactive Input in CalcEngine
     */
    /*
    This is nearly the same as in phase 11, except we have an operation that
    is cross checked wh an enum type
     */
    private static void performOperation2(String[] parts) {

        /*
        - parts[0] is a string, and the method valueOf will attempt
        - to match that string w the right MathOperation Enum
        - To really ensure the string can match the approp. enum,
        - make it upper case, b/c it is case sensitive
        */
        MathOperation operation = MathOperation.valueOf(parts[0].toUpperCase());

        /*
        - because the input is read as a string, parts is
        actually a string array we need to convert that string to a
        - double
        -
         */
        double leftVal = Double.parseDouble(parts[1]);

        double rightVal = Double.parseDouble(parts[2]);

        /*
        Now can call the createCalculation method of type Calculate Base
         */
        CalculateBase calculation = createCalculation(operation, leftVal, rightVal);

        calculation.calculate();

        /*
        Now in this method maybe we want to report some results
         */
        System.out.println("Operation performed: " + operation);
        System.out.println("Result " + calculation.getResult());
    }

    static void handleWhen(String[] parts) {

        LocalDate startDate = LocalDate.parse(parts[1]);

        // cast explicitly to a long
        long daysToAdd = (long) valueFromWord(parts[2]);

        LocalDate newDate = startDate.plusDays(daysToAdd);

        String output = String.format("%s plus %d days is %s", startDate, daysToAdd, newDate);
        //%s will take date times of type LocalDate and aut-turn them into a string

        System.out.println(output);
    }

    /*
    Phase 12: enhanced to receive numeric values as numbers
     */
    static double valueFromWord(String word) {
        /*
        these words are for 10 digits
        This is an array with all the matching
        words for the numbers available
        */

        /*
        This first portion allows for matching of your basic words.
        Next, we want to also be able to handle digits
         */

        String[] numberWords = {
                "zero", "one", "two", "three", "four",
                "five", "six", "seven", "eight", "nine"
        };

        //now the numeric value
        double value = -1d; //purposefully start with an invalid number
        for (int index = 0; index < numberWords.length; index++) {
            if(word.equals(numberWords[index])) {
                value = index;
                break; //break out of the loop
            }
        }

        //no matching happend from string we received
        if(value == -1) {
            //changes string to Double.
            value = Double.parseDouble(word);
        }

        return value;
    }

    /*
    PHASE 19: Class Inheritance
     */

    static void doCalculation(CalculateBase calculation, double leftVal, double rightVal) {
        calculation.setLeftVal(leftVal);
        calculation.setRightVal(rightVal);
        calculation.calculate();
        /*
        Although calculate base calculate method doesn't do anything
        we are depending on the other methods that extend that
        class to override and take over with their well-calculated
        methods
         */
        System.out.println("Calculate result = " + calculation.getResult());
    }

}
