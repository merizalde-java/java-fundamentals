package com.pluralsight.calcengine;

/*
Phase 23: Interfaces
Phase 25: default method
 */
public interface MathProcessing {
    /*
    provide members for interface
     */

    //separator used to break apart value user types in
    //This is essentially a static final field
    String SEPARATOR = " ";

    //to plug in new components, components need to
    // identify the keyword associated with
    String getKeyWord();

    //We want to allow two values that then are used in
    //the actual calculation WITHOUT KNOWING what the
    //calculation is or the actual details of implementation
    // getKeyWord will identify that calculation type for us
    // the details of this are also implemented somewhere else!
    double doCalculation(double leftVal, double rightVal);

    //START ANEW METHOD which many classes have had almost no implementation on
    //DEFAULT METHOD:
    default String getFormattedOutput() {
        return null;
    }

}
