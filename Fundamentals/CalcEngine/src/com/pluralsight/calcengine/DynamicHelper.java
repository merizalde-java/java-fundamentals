package com.pluralsight.calcengine;

/*
Phase 23: Interfaces
 */
public class DynamicHelper {

    //final field
    private final MathProcessing[] handlers;

    //constructor
    public DynamicHelper(MathProcessing[] handlers){
        this.handlers = handlers;
    }

    //methods
    public void process(String statement){
        //CALLS IN THE SEPARATOR DEFINED IN THE INTERFACE
        //altho DynamicHelper does not implement Mathprocessing,
        //any of the actual handlers do!
        //SEPARATOR is essentially a static final field
        String[] parts = statement.split(MathProcessing.SEPARATOR);

        //take the keyword from the statement
        String keyword = parts[0];

        //take left and right values from the user input, which are strings
        //convert to double using parseDouble method from
        //wrapper class Double
        double leftVal = Double.parseDouble(parts[1]);
        double rightVal = Double.parseDouble(parts[2]);

        /*
        Define the single handler we are going to active based on user input
        this is done by walking thru list of handlers
        then taking the handler keyword and matching it with the entered keyword
        from the user input.
        once a match is complete, break the loop
         */
        MathProcessing theHandler = null;
        for(MathProcessing handler : handlers) {
                if(keyword.equalsIgnoreCase(handler.getKeyWord())){
                    theHandler = handler;
                    break;
                }
        }

        double result = theHandler.doCalculation(leftVal, rightVal);
        System.out.println("result = " + result);

    }

}
