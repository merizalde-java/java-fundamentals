package com.pluralsight.calcengine;

/*
Phase 23: Interfaces
 */
public class Adder extends CalculateBase implements MathProcessing{

    /*
    Phase 20: abstract and constructor interface
    - constructors are not automatically inherited
    - by derived classes, outside of the default constructor
    - which is automatic, more or less.
     */

    public Adder(){}

    public Adder(double leftVal, double rightVal){
        //use the keyword super to call in the base
        //class constructor!
        super(leftVal, rightVal);
    }

    //to override we add this annotation to tell Java
    //that this will override the CalcBase annotate
    @Override
    public void calculate() {

        double value = getLeftVal()+getRightVal();
        setResult(value);

    }

    /*
    Phase 23: Interfaces
    */
    /*
    This adder class now implements the work but conforms to contract
    of MathProcessing
     */
    @Override
    public String getKeyWord() {
        return "add";
    }

    @Override
    public double doCalculation(double leftVal, double rightVal) {

        setLeftVal(leftVal);
        setRightVal(rightVal);
        calculate();

        return getResult();
    }
}
