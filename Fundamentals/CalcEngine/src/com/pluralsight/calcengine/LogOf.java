package com.pluralsight.calcengine;

/*
I built this one myself to test out that interface
 */
public class LogOf implements MathProcessing{

    @Override
    public String getKeyWord() {
        return "log";
    }

    @Override
    public double doCalculation(double leftVal, double rightVal) {

        //Check if the values are valid for the problem of taking logarithms
        return (leftVal>0 && rightVal>0)? Math.log(leftVal)/Math.log(rightVal) : 0;
    }
}
