public class Main {

    public static void main(String[] args) {

        /*
        Can't declare a method INSIDE ANOTHER METHOD!!!
        this leads to compile errors:
        https://coderanch.com/t/623695/java/error-semicolon-expected
         */

        /*
        phase 1: methods
        introduction to static
         */
//        this call can be done in the main method
//        the doSomething method is defined OUTSIDE of this method
//        System.out.println("Before method call");
//        doSomething();
//        System.out.println("After method call");

        /*
        phase 2: using parameters
        */
//        call the second method
//        showSum(0.1f, 0.2f, 1);

        /*
        phase 3:  parameter passing behaviour
         */
//        int val1 = 10, val2 = 20;
//        swap(val1, val2);
//        //even if we perform the method, the variables
//        // haven't actually been swapped outside the method so
//        // it looks as if nothing happened.
//        System.out.println("val1: " + val1 + " val2: " + val2);

        /*
        phase 4: exiting a method
        */
//        //case 1: exit method no more code
//        showSum(7.5f, 1.4f, 3);
//        System.out.println("Back from showSum");
//
//        //case 2: exit method using return;
//            //this is using an overridden?/stacked method
//            showSum(7.5f, 1.4d, 3);
//            System.out.println("Back from showSum");
//
//            //this uses the method but doesn't return anything
//            // this is ex of wasted work INSIDE the method
//            showSum(7.5f, 1.4d, 0);
//            System.out.println("Back from showSum");
//
//            //showSumCountChecker changes the logic to reduce the computation
//            //in showSum
//            showSumCountChecker(7.5f, 1.4d, 0);
//            System.out.println("Back from showSumCountChecker");

        /*
        Phase 5: Returning a Value
         */
        double result = calculateInterest(100.0d, 0.05d, 10);
        System.out.println(result);

        double[] resultInterestHistory = produceInterestHistory(100.0d, 0.05d, 10);
        //print out each value one at a time
        for(int idx=0; idx<10; idx++)
            System.out.println(resultInterestHistory[idx]);

    }

    //STATIC modifier
    static void doSomething() {
        System.out.println("Inside method");
        System.out.println("Still inside");
    }

    /*
     phase 2: using parameters
    */
    static void showSum(float x, float y, int count) {
        float sum = x + y;

        for(int i =0; i < count; i++)
            System.out.println(sum);
    }

    /*
    phase 3:  parameter passing behaviour
    */
    static void swap(int i, int j) {
       //values here are copied over to new instances

        //body:
        int k = i;
        i = j;
        j = k;
    }

    /*
    phase 4: exiting a method
     */
    static void showSum(float x, double y, int count) {
        /*
        //guts of this are incredibly similar to the first with the exception
        //that I'm stacking method names and changing via a double for y
        //I am also using a return statement
        */

        double sum = x + y;

        for(int i =0; i < count; i++)
            System.out.println(sum);
        return;
    }

    static void showSumCountChecker(float x, double y, int count) {
        /*
        //guts of this are incredibly similar to the other showsum methods
        logic is changed to reduce compu work within the method
        */
        if (count < 1) {
            //this return statement means we cut out the method before
            // any wasted work is done
            System.out.println("Count < 1! saved some compu work");
            return;
        }
        else {

            double sum = x + y;

            for (int i = 0; i < count; i++)
                System.out.println(sum);
            return;
        }
    }

    /*
    Phase 5: returning a value
     */
    static double calculateInterest(double amt, double rate, int years) {

        double insterest = amt*rate*years;

        // return insterest;
        /*
        we can also just return the calculation
        */
        return amt*rate*years;
    }

    static double[] produceInterestHistory(double principle, double rate, int years) {

        //initiate a new array of type double with size years
        double[] accumulatedInterest = new double[years];

        for(int idxYear = 0; idxYear < years; idxYear++) {
            int year = idxYear+1;
            accumulatedInterest[idxYear] = calculateInterest(principle, rate, year);
        }

        return accumulatedInterest;
    }


}

