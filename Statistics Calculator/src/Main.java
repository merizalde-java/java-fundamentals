import java.util.Arrays;

import static java.lang.Math.pow;

/**
 Author: Jaime Lopez-Merizalde
 Date: 19 10 2020
 Last Update: 19 10 2020
 Title: statistics calculator
 Descripition:
    This code runs multiple statistical calculations such as mean standard deviation and more!
    REPO:
    NOTES:written for JDK 11.0 with INTELLIJ COMMUNITY 2020.2.3
 */

public class Main {

    public static void main(String[] args) {

        // logical paths for arguement entry
        if (args.length == 0) {
            System.out.println("enter some arguements. default not supported yet!");
        } else if (args.length == 1) {
            System.out.println("Only entered the command arguement or not at all, calculation needs at least 1 data arguement.");
        } else if(args.length > 1); //take into account an array of numbers
            handleCommandLineData(args);
    }

    /**
     Methods
     */

    private static void handleCommandLineData(String[] args) {

        //need to define the type of statistic we are doing
        String operation = args[0];

        //setup array of data numerically given the string entered
        double[] rawNumericData = dataParsertoDouble(Arrays.copyOfRange(args, 1, args.length));

        double statisticResult = executeOperation(operation, rawNumericData);

        System.out.println("STATISTIC RESULT: "+ operation + " " + statisticResult);
    }

    private static double[] dataParsertoDouble(String[] stringData) {

        double[] rawNumericData = new double[stringData.length];

        for (int idx = 0; idx < stringData.length; idx++) {
            rawNumericData[idx] = Double.parseDouble(stringData[idx]);
        }

        return rawNumericData;
    }


    private static Double executeOperation(String operation, double[] rawNumericData) {

        double statisticResult;

        //switch through the operations
        switch (operation) {
            case "mean" :
                statisticResult = meanCalculator(rawNumericData);
                break;
            case "median" :
                statisticResult = medianCalculator(rawNumericData);
                break;
//            case "mode" :
//                statisticResult = modeCalculator(rawNumericData);
//                break;
            case "standev" :
                statisticResult = standevCalculator(rawNumericData);
                break;
            case "range" :
                statisticResult = rangeCalculator(rawNumericData);
                break;
            default:
                System.out.println("Invalid operation: " + operation);
                statisticResult = 0.0d;
                break;
        }
        return statisticResult;
    }

    /**
    Measures of centrality
     */
    private static Double meanCalculator(double[] rawNumericData){

        double mean;
        double sum = 0.0d;

        for(int idxData = 0; idxData < rawNumericData.length; idxData++) {
            sum += rawNumericData[idxData];
        }
        mean = sum / rawNumericData.length;

        return mean;
    }

    private static Double medianCalculator(double[] rawNumericData){

        double median;

        //orderTheValues
        Arrays.sort(rawNumericData);

        // odd even tests
        if (rawNumericData.length%2 == 1) {
            int idxMedian = (rawNumericData.length+1 )/ 2;
            return rawNumericData[idxMedian] ;

        } else {
            int idxMedian1 = (rawNumericData.length)/ 2-1;
            int idxMedian2 = idxMedian1+1;
            median =0.5*(rawNumericData[idxMedian1] + rawNumericData[idxMedian2]);
            return median;
        }
    }

    /**
     Measures of variability
     * @param rawNumericData
     * @return

     */
    private static double standevCalculator(double[] rawNumericData) {
        double standev;

        if (rawNumericData.length==1) {
            standev = 0;
        } else {
            double mean = meanCalculator(rawNumericData);
            double sum = 0;
            for(int idx = 0; idx <rawNumericData.length; idx++ ){
                sum +=  pow(rawNumericData[idx] - mean,2);
            }
            standev = sum/(rawNumericData.length-1);
        }

        return standev;
    }

    private static double rangeCalculator(double[] rawNumericData) {
        double range;

        Arrays.sort(rawNumericData);
//        if (rawNumericData.length == 1) {
//            range = 0;
//        } else {
            range = rawNumericData[rawNumericData.length-1] - rawNumericData[0];
//        }

        return range;
    }


}
