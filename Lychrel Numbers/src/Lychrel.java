/*
Author: Jaime Lopez-Merizalde
Date: 15 Nov 2020
UpD: 19 Nov 2020
Descri: reverse and add Lychrel process
 */

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import MathArrayUtilities.*;

public class Lychrel {

    //For numbers under 10k, there are only 50 iterations necessary
    // to produce a palindrome or not

    static final int LYCHREL_ITERATIONS_LIMIT = 50;

    private static int totalLychrelNumbers = 0;
    private static int totalNormieNumbers = 0;
    private static BigInteger tenAsBigInt = new BigInteger("10");

    private ArrayList<BigInteger> lychrelTrace = new ArrayList<BigInteger>();

    private boolean isLychrelNumber = true; //assume Lychrel until otherwise
    private int lychrelCounter = 0;

     //private int number; //this is the number to process on

    /*
    getters
     */
    public int getLychrelIterationLimit(){ return LYCHREL_ITERATIONS_LIMIT; }
    public boolean getIsLychrelNumber(){ return isLychrelNumber; }
    public int getTotalLychrelNumbers(){ return totalLychrelNumbers; }
    public int getTotalNormieNumbers(){ return totalNormieNumbers; }
    public int getLychrelCounter(){ return lychrelCounter; }
    public ArrayList<BigInteger> getLychrelTrace(){ return lychrelTrace; }

    /*
    Methods
     */

    /*
    One such future overloaded method:
    public int[] digitsBreakDown(long number) {
        final int DIGIT_TOL = numberDigitsNeeded(number);
        int[] residualDigits = new int[DIGIT_TOL];

        int idxDigit = 0;
         while( idxDigit < DIGIT_TOL ){
            residualDigits[idxDigit]
                    = (int) (number%Math.pow(10, idxDigit+1) - number%Math.pow(10, idxDigit))/Math.pow(10,idxDigit)
         }
         return residualDigits;
     }
     */

    /*
    Another overloaded version for integers
    public int[] digitsBreakDown(int number) {
        final int DIGIT_TOL = numberDigitsNeeded(number);
        int[] residualDigits = new int[DIGIT_TOL];

        int idxDigit = 0;
         while( idxDigit < DIGIT_TOL ){
            residualDigits[idxDigit]
                    = (int) (number%Math.pow(10, idxDigit+1) - number%Math.pow(10, idxDigit))/Math.pow(10,idxDigit)
         }
         return residualDigits;
     }
     */

    //Overloaded: not everything should immediately be done as a big Integer
    //This should be reserved for when numbers get INCREDIBLY large, which can happen in this process
    private BigInteger[] digitsBreakDown(BigInteger number) {

        final int DIGIT_TOL = numberDigitsNeeded(number);

        BigInteger[] residualDigits = new BigInteger[DIGIT_TOL];

        int idxDigit = 0;

        while( idxDigit < DIGIT_TOL ){
            residualDigits[idxDigit] = number.mod(tenAsBigInt.pow(idxDigit+1))
                            .subtract( number.mod(tenAsBigInt.pow(idxDigit)))
                            .divide(tenAsBigInt.pow(idxDigit));
            idxDigit++;
        }

        return residualDigits;
    }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       

    private BigInteger unDigitize(BigInteger[] digitizedNumber){

        BigInteger undigitizedNumber = new BigInteger("0");

        for(int idx = 0; idx < digitizedNumber.length; idx++){
            undigitizedNumber = new BigInteger(undigitizedNumber.add(digitizedNumber[idx].multiply(tenAsBigInt.pow(idx))).toString());
        }
        return undigitizedNumber;

    }

    private int numberDigitsNeeded(BigInteger number){

        int digitsNeeded = 1;

        while( number.subtract( tenAsBigInt.pow(digitsNeeded) ).compareTo(BigInteger.valueOf(0)) >= 0 ){
            digitsNeeded++;
        }

        return digitsNeeded;
    }

    private BigInteger[] numberFlipper(BigInteger[] digitizedNumber){

        //now flip the number
        BigInteger[] flippedNumber = new BigInteger[digitizedNumber.length];
        for( int idx1 = 0; idx1 < digitizedNumber.length; idx1++ ){
            flippedNumber[idx1] = digitizedNumber[digitizedNumber.length - idx1 - 1];
        }

        return flippedNumber;
    }

    //Overloaded
    public boolean isPalindrome(BigInteger number){

        //first digitize this number
        BigInteger[] digitizedNumber = digitsBreakDown(number);

        BigInteger[] flippedNumber = numberFlipper(digitizedNumber);

        if (Arrays.equals(digitizedNumber, flippedNumber)) {
            return true;
        }
        else
            return false;
    }

    //Overloaded
    public boolean isPalindrome(BigInteger[] number){

        return isPalindrome(unDigitize(number));

    }

    private BigInteger LychrelMethod(BigInteger number){

        return unDigitize(MathTools.addTwoArrays( digitsBreakDown(number),
                numberFlipper(digitsBreakDown(number))) );
    }

    public void LychrelProcess(int number){

        //vm vert to BigInteger: This is a little extreme, and should be evaulated before hand to decide if
        //a big integer is needed. For now, we are using the nuclear option.
        BigInteger lychrelNumber = BigInteger.valueOf(number);

        do{
            lychrelNumber = LychrelMethod(lychrelNumber);
            lychrelTrace.add(lychrelCounter, lychrelNumber);
            lychrelCounter++;

        }while( !isPalindrome(lychrelNumber) &&  lychrelCounter <= LYCHREL_ITERATIONS_LIMIT);

        if(isPalindrome(lychrelNumber)){
            isLychrelNumber = false;
            totalNormieNumbers++;
        }
        else { //found a Lychrell number, add to total
            totalLychrelNumbers++;
        }
    }

}