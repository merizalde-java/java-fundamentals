/*
Author: Jaime Lopez-Merizalde
Date: 15 Nov 2020
UpD: 19 Nov 2020
Descri: Finds all reversible numbers below 10^9
https://projecteuler.net/problem=55
 */

import MathArrayUtilities.MathTools;

public class Main {

    public static void main(String[] args) {

//        testLychrelProblem();

        executeLychrelProblem();
    }

    private static void testLychrelProblem() {

        /*
        A few values to test:
        Palindrome Numbers: 20, 1371, 3534
        Lychrel Numbers: 196, 1497, 2496, 4994, 9687
         */
        
        int[] numbersArray = {20, 196};

        //diag
        for (int idxIndxToTest = 0; idxIndxToTest < numbersArray.length; idxIndxToTest++ ){

            Lychrel LychrelProblem = new Lychrel();
            LychrelProblem.LychrelProcess( numbersArray[idxIndxToTest] );

            System.out.println( "Number testing: " + numbersArray[idxIndxToTest]
                    + ". Is Lychrel number: " + LychrelProblem.getIsLychrelNumber()
                    + ". Trace: " + LychrelProblem.getLychrelTrace() + ". Iterations "
                    + LychrelProblem.getLychrelCounter());
            System.out.println();
        }

    }

    /*
    Methods
     */
    public static void executeLychrelProblem(){
        int[] numbersArray = MathTools.consecutiveIntegers(10000);

        for (int idxNumbersArray = 0; idxNumbersArray < numbersArray.length; idxNumbersArray++){
            Lychrel LychrelProblem = new Lychrel();

            LychrelProblem.LychrelProcess( numbersArray[idxNumbersArray] );
            System.out.println( "Number testing: " + numbersArray[idxNumbersArray]
                    + ". Is Lychrel number: " + LychrelProblem.getIsLychrelNumber()
                    + ".  Trace: " + LychrelProblem.getLychrelTrace() + ". Iterations "
                    + LychrelProblem.getLychrelCounter());
        }

        System.out.println();
        System.out.println("Total Lychrel Numbers: " + new Lychrel().getTotalLychrelNumbers());
        System.out.println("Total Palindrome Numbers: " + new Lychrel().getTotalNormieNumbers());

    }
}

