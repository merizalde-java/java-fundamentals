package MathArrayUtilities;

import java.math.BigInteger;

public class MathTools {

    public static int[] addTwoArrays(int[] array1, int[] array2){

        if( array1.length != array2.length ){
            System.out.println("Arrays of unequal length");
            return new int[0];
        }
        else {
            int[] sumArray = new int[array1.length];

            for (int idx1 = 0; idx1 < array1.length; idx1++){
                sumArray[idx1] = array1[idx1] + array2[idx1];
            }
            return sumArray;
        }
    }

    public static BigInteger[] addTwoArrays(BigInteger[] array1, BigInteger[] array2){

        if( array1.length != array2.length ){
            System.out.println("Arrays of unequal length");
            return new BigInteger[0];
        }
        else {
            BigInteger[] sumArray = new BigInteger[array1.length];

            for (int idx1 = 0; idx1 < array1.length; idx1++){
                sumArray[idx1] = array1[idx1].add(array2[idx1]);
            }
            return sumArray;
        }
    }

    public static int[] consecutiveIntegers(int finalInt){

        int[] arrayConsecInts = new int[finalInt];
        arrayConsecInts[0] = 0;
        for(int idxInt = 1; idxInt < finalInt; idxInt++){
            arrayConsecInts[idxInt] = arrayConsecInts[idxInt-1]+1;
        }

        return arrayConsecInts;
    }
}
