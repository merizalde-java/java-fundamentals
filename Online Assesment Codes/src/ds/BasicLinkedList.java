package ds;

public class BasicLinkedList<X>{

    //Fields:
    // for the linked list data structure
    private Node first;
    private Node last;
    private int nodeCount;

    /*
    Constructor:
    instantiating a basic linked list
    always has to have first and last nodes as nulls;
     */
    public BasicLinkedList(){
        first = null;
        last = null;
        nodeCount = 0;
    }

    /*
    Inner classes
     */

    //Private node class
    // this is all the 'cars'
    private class Node {
        //fields: 2 fields for node to specify nextnode
        //and the type of node for this node.

        //links node to next node
        private Node nextNode;

        //specifies type of node
        //ex: box car or tanker car
        private X nodeItem;

        //Constructor for this private class
        public Node(X item){
            this.nextNode = null;
            this.nodeItem = item;
        }

        /*
        Getters Setters
         */
        //We can link which node this is linked to
        public void setNextNode(Node nextNode){ this.nextNode = nextNode; }

        public Node getNextNode(){ return nextNode; }

        public X getNodeItem(){ return nodeItem; }

    }

    /*
    Methods
     */

    // Size: track linked list size.
    // Integer type.
    public int size(){
        return nodeCount;
    }

    // Add
    // appends new item on end of list

    public void add(X newItem){

        //Check current status of linked list
        if(first == null){
            first = new Node(newItem);
            last = first;
        }
        else {
            //instantiate a new node
            Node newNode = new Node(newItem);

            //reset last node pointer
            last.setNextNode(newNode);
            last = newNode;
        }

        //update node count
        nodeCount++;
    }

    // Remove
    // pull the first node off the list

    public X remove(){

        /*
        Note: for this code. notice no looping was performed.
        Just upated some pointers of our list. This keeps the operation at constant time.
        If this is all we had, we would be equivalent to a Q.
         */
        if (first == null){
            //the teacher chose to throw an exception.
            throw new IllegalStateException("Empty LinkedList! Cannot remove an item.");
        }
        else {
            //store the  soon to be removed node
            Node firstNodeRemoved = first;
            //set the next node as the first node. could be null. doesn't matter
            first = first.nextNode;
            nodeCount--;
            return firstNodeRemoved.nodeItem;
        }
    }

    // Insert
    // insert item to this list at specified location along list
    public void insert(X item, int position){

        //check first if list has nothing in it
        //if attempting to add at the end of a list
        if (position > nodeCount){
            throw new IllegalArgumentException("Attempted to place item in " +
                    "position larger than current list size.");
        }

        else {
            //get to the node starting index position
            //O(n) operation
            Node seekNode = first;
            for(int idx=0; idx<position && seekNode !=null; idx++){
                seekNode = seekNode.nextNode;
            }

            //Break the link and reset
            // first store the current nextnode
            Node postNode =seekNode.getNextNode();
            Node insertingNode = new Node(item);
            // break and set the new next node as the provided insert node
            seekNode.setNextNode(insertingNode);
            // recouple with previously broken link;
            insertingNode.setNextNode(postNode);

            //increment count
            nodeCount++;
        }

    }

    // RemoveAt
    public X removeAt(int position){
        // check null list
        if (first == null){
            throw new IllegalStateException("LinkList is empty. Cannot remove anything.");
        }
        else if (position >= size()){
            throw new IllegalArgumentException("Given position greater than LinkList size.");
        }
        else {
            // Get to the node starting position to be removed
            // O(n) operation
            Node currentNode = first;
            Node prevNode = first;

            for(int idx = 1; idx<position && currentNode != null; idx++ ){
                prevNode = currentNode;
                currentNode = currentNode.getNextNode();
            }

            prevNode.setNextNode(currentNode.getNextNode());

            nodeCount--;
            return currentNode.nodeItem;
        }
    }

    // Get
    // get an item at a position
    public X get(int position){
        if (first == null){
            throw new IllegalStateException("No elements to get in LinkedList.");
        }

        Node currentNode = first;
        for(int idx = 1; idx < size() && currentNode != null; idx++){
            if(idx == position){
                return currentNode.getNodeItem();
            }
            currentNode = currentNode.getNextNode();
        }
        return null;
    }

    // Find
    // returns position of item. this is the dual of get
    public int find(X item){
        if (first == null){
            throw new IllegalStateException("Cannot find item in empty LinkedList.");
        }

        Node currentNode = first;
        for(int idx = 1; idx < size() && currentNode != null; idx++){
            //do equality check with equals method or you may not be doing
            // the appropriate logic for this
            if ( currentNode.getNodeItem().equals(item) ){
                return idx;
            }
        }

        return -1;
    }

    // ToString
    // returns data as string
    @Override
    public String toString(){

        //Instantiate the StringBuffer;
        StringBuffer contents = new StringBuffer();

        // Instant @ current node as first;
        Node currentNode = first;

        // Loop through Basic Linked List to build a string for contents inside
        while(currentNode != null){
            // append NodeItem contents to string buffer
            contents.append(currentNode.getNodeItem());

            // update currentNode
            currentNode = currentNode.getNextNode();

            // comma separated values
            if( currentNode != null){
                contents.append(", ");
            }
        }

        // StringBuffer class has a toString method so we apply.
        return contents.toString();
    }
}
