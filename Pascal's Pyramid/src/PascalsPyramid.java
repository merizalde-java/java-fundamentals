/*
Author: Jaime Lopez-Merizalde, Ph.D.
Date: 14 Jan 21
UpD: 16 Feb 21
Descri: makes an array storing pyramid values
 */

import java.math.BigInteger;
import java.util.ArrayList;

public class PascalsPyramid {

    private static int divisibleCoefficients = 0;
    private static final int MAX_DIVISOR = 10000;

    /*
    //Lists and ArrayList do not allow primitive types because they are not objects
    They are collections of objects, which prim types are not.
    Sometimes autoboxing happens, where prim types are autoboxed to
    initialize the equivalent object version with the same values
     */

    private ArrayList<Object> pascalTriangleHistory = new ArrayList<Object>(1);
    private ArrayList<Object> multiplierList = new ArrayList<Object>(1);
    private ArrayList<Integer> symmetryList = new ArrayList<Integer>(1);
    private int[] indexingArray;
    private int[] pascalRowGroupSize;
    public ArrayList<Object> pascalPyramid;
    public ArrayList<Object> pascalPyramidFull;

    MixedTypeCalculator MTC = new MixedTypeCalculator();

    /*
    Getters, Setters
     */
    public ArrayList<Object> getPascalTriangleHistory() { return pascalTriangleHistory; }

    public ArrayList<Integer> getSymmetryList() { return symmetryList; }

    public ArrayList<Object> getMultiplierList() { return multiplierList; }

    public ArrayList<Object> getPascalPyramid() { return pascalPyramid; }

    public ArrayList<Object> getPascalPyramidFull() { return pascalPyramidFull; }

    public int[] getIndexingArray() { return indexingArray; }

    public int[] getPascalRowGroupSize() { return pascalRowGroupSize; }

    public int getDivisibleCoefficients() { return divisibleCoefficients; }

    public int getMaxDivisor(){ return MAX_DIVISOR; }

    /*
    Constructors
    */
    public PascalsPyramid() { pascalTriangleHistory.add(1); }

    /*
    Methods
     */

    public void CreatePascalPyramidCofactors(int pascalLevel) {
        //diag: under investigation
        CreatePascalTriangleHistory(pascalLevel); //diag: should be okay
        CreateSymmetryList(pascalLevel); //diag: should be okay
        CreateMultiplierList(pascalLevel); //diag: should be okay
        CreateIndexingArray(pascalRowGroupSize);
        CreatePascalPyramid();


        //diag: dont' do the full pyramid. only putting it here for visual verification
        //PascalPyramidProcess(pascal);
        //CreatePascalPyramidFull();
    }

    //Pascal Pyramid:
    private void CreatePascalTriangleHistory(int pascalLevel) {

        //If we're at a value n >= 1, then we automatically add the first Pyramid.
        if (pascalLevel >= 1) {
            pascalTriangleHistory.add(1);

            //Set the first expander array as the first expanded pyramid
            ArrayList<Object> currentExpander = new ArrayList<Object>(){{add(pascalTriangleHistory.get(1));}};

            for (int idx = 1; idx < pascalLevel; idx++) {

                //update the expander given previous expansion.
                currentExpander = Expander(currentExpander);

                pascalTriangleHistory.addAll(currentExpander);
            }
        }
    }

    private void CreateMultiplierList(int pascalLevel) {

        for (int idx1 = 0; idx1 <= pascalLevel; idx1++) {

            for (int idx2 = 0; idx2 < idx1 / 2 + 1; idx2++) {
                if (idx1 <= pascalLevel / 2) {
                    multiplierList.add(pascalTriangleHistory.get(pascalTriangleHistory.size() - 1 - pascalLevel / 2 + idx1));
                }
                else {
                    multiplierList.add(pascalTriangleHistory.get(pascalTriangleHistory.size() - 1 + (pascalLevel + 1) / 2 - idx1));
                }
            }
        }
    }

    /*
    CreateSymmetryList method:
    this list should take in an integer representing the pascal level, then return the delta list
    counting the number of repeated values in the pascal pyramid due to 3-way symmetry at each level
     */
    private void CreateSymmetryList(int pascalLevel) {

        /*
        See notes for reason behind implementation
         */
        boolean triple;
        if (pascalLevel % 3 == 0) {
            triple = true;
        } else {
            triple = false;
        }

        int maxIterations = pascalLevel / 3 + 1; //corresponds to k in notes
        int largestRow = pascalLevel + 1; //corresponds to n+1 in notes

        boolean parity = true;
        //pascal row group size elements are at most pascalLevel/2
        // so as long as the software limits the pascal level appropriately, we should be okay
        pascalRowGroupSize = new int[maxIterations];
        for (int idx = 0; idx < maxIterations; idx++) {

            //Parity
            if (idx == 0) {
                if (largestRow % 2 == 0) {
                    parity = true;
                } else {
                    parity = false;
                }
            } else {
                parity = !parity;
            }

            //pascalRowGroupSize
            pascalRowGroupSize[idx] = (int) Math.ceil((largestRow - 3 * idx) / 2.0);

            //Filler
            if (pascalRowGroupSize[idx] == 1) {
                if (triple) {
                    symmetryList.add(1);
                } else {
                    symmetryList.add(3);
                }
            } else {
                SymmetryListExpander(parity, pascalRowGroupSize[idx]);
            }
        }
    }

    /*
    CreatePermutationArray:
    See 1 Feb 2021 Notes for background
     */
    private void CreateIndexingArray(int[] pascalRowGroupSize) {

        int sizePascalRowGroupSizeSize = pascalRowGroupSize.length;

        //Find sum of pascalRowGroupSize
        int[] sumPascalRowGroupSize = new int[pascalRowGroupSize.length + 1];

        for (int idx1 = 1; idx1 <= sizePascalRowGroupSizeSize; idx1++) {
            sumPascalRowGroupSize[idx1] = sumPascalRowGroupSize[idx1 - 1] + pascalRowGroupSize[idx1 - 1];
        }

        indexingArray = new int[sumPascalRowGroupSize[sumPascalRowGroupSize.length - 1]];

        for (int idx1 = 0; idx1 < sizePascalRowGroupSizeSize; idx1++) {
            if (idx1 > 0) {
                //Fill in the starting indices for each subGroup
                indexingArray[sumPascalRowGroupSize[idx1]] = (idx1) * (idx1 + 2);

                //Fill in the rest of the array
                for (int idx2 = 1 + sumPascalRowGroupSize[idx1]; idx2 < pascalRowGroupSize[idx1] + sumPascalRowGroupSize[idx1]; idx2++) {
                    indexingArray[idx2] = indexingArray[idx2 - 1] + idx1 + (int) Math.ceil((idx2 - sumPascalRowGroupSize[idx1]) / 2.0);
                }
            } else {
                indexingArray[idx1] = 0;

                //Fill in the rest of the array
                for (int idx2 = 1 + sumPascalRowGroupSize[idx1]; idx2 < pascalRowGroupSize[idx1] + sumPascalRowGroupSize[idx1]; idx2++) {
                    indexingArray[idx2] = indexingArray[idx2 - 1] + idx1 + (int) Math.ceil((idx2 - sumPascalRowGroupSize[idx1]) / 2.0);
                }
            }
        }

    }

    /*
    CreatePascalPyramid:
    Glues together the pascalTriangleHistory and the Multipler
    List using the indexing array.
     */
    private void CreatePascalPyramid() {

        pascalPyramid =new ArrayList<Object>();

        //get the list started. first value is always 1;
        pascalPyramid.add(1);

        for( int idx1 = 1; idx1<indexingArray.length; idx1 ++ ) {

            //This is a problem. the indexing array may exceed what's possible for lists, arrays.....
             pascalPyramid.add(idx1, MTC.MathMultiply(pascalTriangleHistory.get(indexingArray[idx1]),
                    multiplierList.get(indexingArray[idx1])));
        }
    }

    private void CreatePascalPyramidFull(){

        //Warning: this should rarely if ever be used. For debugging only!
        System.out.println("CreatePascalPyramidFull is Active. Only use if debugging!");

        pascalPyramidFull =new ArrayList<Object>();
        //get the list started. first value is always 1;
        pascalPyramidFull.add(1);

        for( int idx1 = 1; idx1<pascalTriangleHistory.size(); idx1 ++ ) {
            //DIAG: this is a problem. the indexing array may exceed what's possible for lists, arrays......
            pascalPyramidFull.add(idx1, MTC.MathMultiply(pascalTriangleHistory.get(idx1),
                    multiplierList.get(idx1)));
        }
    }

    /*
    Expands a given pascal triangle array into the next iteration.
     */
    private ArrayList<Object> Expander(ArrayList<Object> currentPascalArray){

        //arraysize - 1 tells you what pascal level is active for this array
        int pascalArrayLength = currentPascalArray.size();
        int pyramidIterator;

        if (pascalArrayLength == 1){
            pyramidIterator = 1;
        }
        else {
            //this is surely an integer at least for the level we are providing this value.
            //n not greater than 200k. Otherwise this would fail.
            pyramidIterator = (int) currentPascalArray.get(1);
        }

        ArrayList<Object> expandedPascalArray;

        if ( pyramidIterator%2 == 1 ){ //odd
            expandedPascalArray = new ArrayList<Object>(pascalArrayLength+1);
            expandedPascalArray.add(1);
        }
        else {
            expandedPascalArray = new ArrayList<Object>(pascalArrayLength);
            expandedPascalArray.add(1);
        }

        for (int idx1 = 1; idx1 < pascalArrayLength; idx1++){
            expandedPascalArray.add( MTC.MathAdd(currentPascalArray.get(idx1-1), currentPascalArray.get(idx1)));
        }

        if ( pyramidIterator%2 == 1 ) { //odd
            expandedPascalArray.add(MTC.MathMultiply(2, currentPascalArray.get(pascalArrayLength - 1)));
        }

        return expandedPascalArray;

    }

    private void SymmetryListExpander(boolean parity, int pascalRowGroupSize){
        //adds the first value (appears 3 times in the pyramid)
        symmetryList.add(3);

        //adds the values which appear 6 times in the pyramid
        for( int idx = 1; idx < pascalRowGroupSize - 1; idx++){
            symmetryList.add(6);
        }

        //adds final value: appears either 6 or 3 times depending on parity
        if( parity == false ){ //odd parity
            symmetryList.add(3);
        }
        else { //even parity
            symmetryList.add(6);
        }
    }

    /*
    Divisible Coefficients Process: Overloaded Method
     */
    public void DivisibleCoefficientsProcess(int pascalLevel, int divisor){

        //Don't divide by zero, don't perform any pascal operations for a non-sense level
        if( pascalLevel < 100 && divisor < MAX_DIVISOR && 0 < divisor ){

            CreatePascalPyramidCofactors(pascalLevel);
            DivisibilityCounter( divisor );
        }

        else if ( pascalLevel <= 500 && divisor < MAX_DIVISOR ){
            CreatePascalPyramidCofactors(pascalLevel);
            DivisibilityCounter( divisor );
        }
        else if( divisor >= MAX_DIVISOR ){

            //Warning message: if we provide a large integer, we will only do work if it's a power of 10
            System.out.println("Warning: provided divisor > MAX_DIVISOR: " + MAX_DIVISOR);

            double power = Math.log10(divisor);

            //Only do work if we have power of 10
            //Test if the result is an integer.
            if( power == (int) power ){
                //Warning message
                System.out.println("Warning: we are only proceeding because divisor is power of 10.");

                CreatePascalPyramidCofactors(pascalLevel);
                DivisibilityCounter((int) power, true );
            }
            else{
                System.out.println("Warning: provided divisor not power of 10.");
            }

        }
        else {
            System.out.println("Pascal Level might be too large.");
        }
    }

    /*
    Divisible Coefficients Process: Overloaded Method 2
    */
    public void DivisibleCoefficientsProcess(int pascalLevel, long divisor){

        System.out.println("Warning: Using a long divisor, which is reserved for  cases of divisor > 4E9");
        if( divisor >= 4*Math.pow(10,9) ){

            double power = Math.log10(divisor);

            //Only do work if we have power of 10:
            // the power is okay as an integer, it won't exceed 12.
            if( power == (int) power ){
                //Warning message
                System.out.println("Warning: we are only proceeding because this is power of 10.");

                CreatePascalPyramidCofactors(pascalLevel);
                DivisibilityCounter((int) power, true );
            }
            else{
                System.out.println("Warning: provided divisor not power of 10.");
            }

        }
        else {
            System.out.println("Warning: Did not provide long that was large enough to warrant use of divisiblity method.");
        }
    }

    /*
    Divisibility counter method: overloaded 1
     */
    private void DivisibilityCounter( int divisor ){
        for( int idx1 = 0; idx1 < pascalPyramid.size(); idx1++ ){

            //Decode the data type
            String leftValueSimpleName = pascalPyramid.get(idx1).getClass().getSimpleName();

            if (leftValueSimpleName.equals("Integer") && Integer.compare((int) pascalPyramid.get(idx1), divisor) >= 0 && (int) pascalPyramid.get(idx1)%divisor == 0){
                //diag
                //System.out.println("dividing Integer.");

                divisibleCoefficients += symmetryList.get(idx1);
            }
            else if (leftValueSimpleName.equals("Long") && Long.compare((long) pascalPyramid.get(idx1), divisor) >= 0 &&  (long) pascalPyramid.get(idx1)%divisor == 0){
                //diag
                //System.out.println("dividing Long.");
                divisibleCoefficients += symmetryList.get(idx1);
            }
            else if (leftValueSimpleName.equals("BigInteger")){
                //diag
                //System.out.println("dividing BigInteger.");

                BigInteger leftValue = (BigInteger) pascalPyramid.get(idx1);
                if ( leftValue.compareTo(BigInteger.valueOf(divisor)) >= 0 && leftValue.mod(BigInteger.valueOf(divisor)).compareTo(BigInteger.ZERO) == 0){
                    divisibleCoefficients += symmetryList.get(idx1);
                }
            }

        }
    }

    /*
    Divisibility counter method: overloaded 2
     */
    //Note boolean: isPowerTen is only used for its signature signature. the method does not actually use the value necessary.
    private void DivisibilityCounter(int power, boolean isPowerTen){

        for( int idx1 = 0; idx1 < pascalPyramid.size(); idx1++){

            //Decode the data type
            String leftValueSimpleName = pascalPyramid.get(idx1).getClass().getSimpleName();

            //convert value to a string-type:
            if (leftValueSimpleName.equals("Integer")) {

                //convert to string
                String pascalPyramidValueAsString = ((Integer) pascalPyramid.get(idx1)).toString();

                //count the zeros
                int indexCounter = 0, zeroCounter = 0;
                while( zeroCounter < power && Character.compare(pascalPyramidValueAsString.charAt(pascalPyramidValueAsString.length()-indexCounter -1), '0') == 0 ){
                    indexCounter++;
                    zeroCounter++;
                }
                if ( power <= zeroCounter ){
                    divisibleCoefficients += symmetryList.get(idx1);
                }

            }
            else if (leftValueSimpleName.equals("Long")) {

                //Convert to String
                String pascalPyramidValueAsString = ((Long) pascalPyramid.get(idx1)).toString();

                //count the zeros
                int indexCounter = 0, zeroCounter = 0;
                while( zeroCounter < power && Character.compare(pascalPyramidValueAsString.charAt(pascalPyramidValueAsString.length()-indexCounter -1), '0') == 0 ){
                    indexCounter++;
                    zeroCounter++;
                }
                if ( power <= zeroCounter ){
                    divisibleCoefficients += symmetryList.get(idx1);
                }
            }
            else if (leftValueSimpleName.equals("BigInteger")){
                //convert to string
                String pascalPyramidValueAsString = ((BigInteger) pascalPyramid.get(idx1)).toString();

                //count the zeros
                int indexCounter = 0, zeroCounter = 0;

                while( zeroCounter < power && Character.compare(pascalPyramidValueAsString.charAt(pascalPyramidValueAsString.length()-indexCounter - 1), '0') == 0 ){
                    indexCounter++;
                    zeroCounter++;
                }
                if ( power <= zeroCounter ){
                    divisibleCoefficients += symmetryList.get(idx1);
                }
            }
        }
    }

}
