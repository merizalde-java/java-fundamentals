/*
Date: 14 Jan 21
UpD: 18 Feb 21
Descri: Runs manager methods for solving Pascal's Pyramid
 */

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        //subtest
        //TestBigIntegerMethods();

        //subtest
        //TestingMixedTypeCalculator();

        //subtest
        //TestingCreatePascalTriangleHistory();

        //subtest
        //TestCreateMultiplierList();

        //subtest
        //TestCreateIndexingArray();

        //test method
        //TestPascalDivisible();

        //test method
        //TestIsDivisible();

        //test method
        //TestPascalsPyramidHighCapacity();

        //full method
        PascalDivisible();

    }

    private static void TestBigIntegerMethods(){
        BigInteger leftValue = new BigInteger("1200");

        int counter = 0;
        char[] thisnewCarArray = new char[4];
        leftValue.toString().getChars(0,4, thisnewCarArray, 0);
        System.out.println(thisnewCarArray);
    }

    private static void TestingMixedTypeCalculator(){

        //DIAG: MAKE A LIST WITH MISED TYPES
        ArrayList<Object> testList = new ArrayList(1);
        PascalsPyramid pascalsPyramid = new PascalsPyramid();

        MixedTypeCalculator MTC = new MixedTypeCalculator();
        Object result;

        /*
        Some values
         */
        int thisInt = Integer.MAX_VALUE;
        long thisLong = Long.MAX_VALUE;
        BigInteger thisBigInteger = new BigInteger("1000000000000"); // 10^12

        //initialize an array list and
        ArrayList<Object> mixedList = new ArrayList<Object>(){
            {add(thisInt); add(thisLong); add(thisBigInteger);}
        };

        /*
        Some math operations.
        9 Cases total
         */

        //TEST 9 CASES FOR ADDITION
        /*
        for (int idx1 = 0; idx1 < mixedList.size(); idx1++){
            for (int idx2 = 0; idx2 < mixedList.size(); idx2++){
                System.out.println("Operating on: ");
                System.out.println("Left Value: "
                        + mixedList.get(idx1).getClass().getSimpleName()
                        + " Right Value: " + mixedList.get(idx2).getClass().getSimpleName());
                System.out.println(MTC.MathAdd(mixedList.get(idx1), mixedList.get(idx2)));
            }
        }
        */


        //TEST 9 CASES FOR MULTIPLICATION
        /*
        for (int idx1 = 0; idx1 < mixedList.size(); idx1++){
            for (int idx2 = 0; idx2 < mixedList.size(); idx2++){
                System.out.println("Operating on: ");
                System.out.println("Left Value: "
                        + mixedList.get(idx1).getClass().getSimpleName()
                        + " Right Value: " + mixedList.get(idx2).getClass().getSimpleName());
                System.out.println(MTC.MathMultiply(mixedList.get(idx1), mixedList.get(idx2)));
            }
        }
        */
    }

    private static void TestingCreatePascalTriangleHistory(){

        //STATUS: online

        //necessary starters
        PascalsPyramid pascalsPyramid = new PascalsPyramid();

        //executiong of process
        //pascalsPyramid.CreatePascalTriangleHistory(7);

        //What was the result?
        System.out.println("Pascal Pyramid: " + pascalsPyramid.getPascalTriangleHistory());

    }

    private static void TestCreateMultiplierList(){
        //STATUS: ONLINE

        //necessary starters
        PascalsPyramid pascalsPyramid = new PascalsPyramid();


        //execute process
        int pascalLevel = 0;
        //pascalsPyramid.CreatePascalTriangleHistory(pascalLevel);
        //pascalsPyramid.CreateMultiplierList(pascalLevel);

        //What was the result?
        System.out.println("Multiplier List: " + pascalsPyramid.getMultiplierList());
    }

    private static void TestCreateIndexingArray(){
        //STATUS: ONLINE

        //necessary starters
        PascalsPyramid pascalsPyramid = new PascalsPyramid();
        int pascalLevel = 60;

        //execute process
        pascalsPyramid.CreatePascalPyramidCofactors(pascalLevel);

        //result
        System.out.println("Pascal triangle history: " + pascalsPyramid.getPascalTriangleHistory());
        System.out.println("Pascal multiplier list: " + pascalsPyramid.getMultiplierList());
        System.out.println("Pascal Symmetry list: " + pascalsPyramid.getSymmetryList());
        System.out.println("Indexing Array: " + Arrays.toString(pascalsPyramid.getIndexingArray()));
    }

    private static void TestIsDivisible(){
        //STATUS: ONLINE

        //NEEDS
        BigInteger value1 = BigInteger.valueOf(50);
        BigInteger value2 = BigInteger.valueOf(2000);
        int power = 6;
        PascalsPyramidHighCapacity pascalPyramidObject = new PascalsPyramidHighCapacity(10);

        //EXECUTE
        //boolean bool = pascalPyramidObject.IsDivisible(value1, value2, power);

        //RESULTS
        //System.out.println("bool IsDivisible: " + bool);

    }

    private static void TestPascalDivisible() {

        //STATUS: ONLINE

        //necessary starters
        /*
        Note: longs start to appear in PascalPyramid at pascalLevel = 25
        BigIntegers start to appear at pascalLevel = 44
        This shows most pascal pyramid values are big integers
        This is a low-capacity version / run of Pascal's Pyramid calculations which is feasibly for
        pascal Level 0 - 1000.
        For levels beyond this, the calculations become intractable.
        A different more in-place version is used to explore calculations at that level
         */
        int divisor = (int) Math.pow(10,2); //Divisor. MAXInt = 10^9 (nearly)
        //long divisor = (long) Math.pow(10,12);//Our code also does a long
        int pascalLevel = 10; // number of triangle stages to go to. (don't go beyond 2x10^5)

        PascalsPyramid pascalPyramidObject = new PascalsPyramid();

        //execute process
        pascalPyramidObject.DivisibleCoefficientsProcess(pascalLevel, divisor);

        System.out.println("test divisibility: " + "Pascal Level: " + pascalLevel + " divisor: " + divisor +
                " divisible coefficients: " + pascalPyramidObject.getDivisibleCoefficients());

    }

    private static void TestPascalsPyramidHighCapacity(){
        //STATUS: ONLINE

        //DIAG
        final long startTime = System.currentTimeMillis();

        //Need
        int pascalLevel = 10; //
        int divisor = 100; //Run only powers of 10;
        PascalsPyramidHighCapacity pascalsPyramidHighCapacity = new PascalsPyramidHighCapacity(pascalLevel);

        //Execute process
        // pascalsPyramidHighCapacity.PopulatePascalTriangleArray(pascalLevel);
        pascalsPyramidHighCapacity.DivisibilityCounter(pascalLevel, divisor);

        //Results
        if (pascalLevel < 100) {
            System.out.println("PascalTriangleArray: " + Arrays.toString(pascalsPyramidHighCapacity.getPascalTriangleArray()));
        }
        else System.out.println("Do not print PascalTriangleArray when it's too large. Just look at the time it took to calculate it.");

        System.out.println("Divisible Coeffcients: " + pascalsPyramidHighCapacity.getDivisibleCoefficients());

        //DIAG
        final long endTime = System.currentTimeMillis();
        System.out.println("Total execution time (milliSeconds): " + (endTime - startTime));
    }

    private static void PascalDivisible(){
        //STATUS: ONLINE

        //DIAG
        final long startTime = System.currentTimeMillis();

        //Need
        int pascalLevel = 200000; // 200 thousand
        int pascalPower = 12; //10^12
        PascalsPyramidHighCapacity pascalsPyramidHighCapacity = new PascalsPyramidHighCapacity(pascalLevel);

        //Execute process
        pascalsPyramidHighCapacity.DivisibilityCounterLargePower(pascalLevel, pascalPower);

        //Results
        if (pascalLevel < 100) {
            System.out.println("PascalTriangleArray: " + Arrays.toString(pascalsPyramidHighCapacity.getPascalTriangleArray()));
        }
        else System.out.println("Do not print PascalTriangleArray when it's too large. Just look at the time it took to calculate it.");

        System.out.println("Divisible Coeffcients: " + pascalsPyramidHighCapacity.getDivisibleCoefficients());

        //DIAG
        final long endTime = System.currentTimeMillis();
        System.out.println("Total execution time (milliSeconds): " + (endTime - startTime));
    }
}
