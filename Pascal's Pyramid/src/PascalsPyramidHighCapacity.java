/*
Author: Jaime Lopez-Merizalde, Ph.D.
Date: 16 Feb 21
UpD: 17 Feb 21
Descri: calculates pascal's pyramid for values divisible by large divisor
 */

import java.math.BigInteger;
import java.util.Arrays;

public class PascalsPyramidHighCapacity {

    /*
    Fields
    */
    private static int divisibleCoefficients = 0;
    private static int divisorPower;
    private BigInteger[] pascalTriangleArray;

    /*
    Getters Setters
    */
    public int getDivisibleCoefficients(){ return divisibleCoefficients; }
    public int getDivisorPower(){ return divisorPower; }
    public BigInteger[] getPascalTriangleArray(){ return pascalTriangleArray; }

    /*
    Constructor
    */
    public PascalsPyramidHighCapacity(int pascalLevel){
        pascalTriangleArray = new BigInteger[(int) ((pascalLevel + 1)/2 + (pascalLevel + 1)%2)];
    }

    /*
    Methods
    */
    private void AddOneDivisibleCoefficientsCounter(){ divisibleCoefficients++; }
    private void AddTwoDivisibleCoefficientsCounter(){ divisibleCoefficients += 2; }

    //PopulatePascalTriangleArray: will update pascalTriangleArray in place
    private void PopulatePascalTriangleArray(int pascalLevel){

        pascalTriangleArray[0] = BigInteger.ONE;

        for (int idxPascalLevel = 1; idxPascalLevel <= pascalLevel; idxPascalLevel++){
            InPlaceExpander(idxPascalLevel);
        }
    }

    /*
    Overloaded:
    1. just to work on pascalTriangleArray with mininal repeated access
     */
    private void InPlaceExpander(int idxPascalLevel){

        if (idxPascalLevel%2 == 0){
            pascalTriangleArray[idxPascalLevel/2] = pascalTriangleArray[(idxPascalLevel-1)/2].multiply(BigInteger.TWO);
        }

        //filling this in backwards
        for(int idx2 = (idxPascalLevel - 1)/2 - 1; idx2 >= 0 ; idx2--){
            pascalTriangleArray[idx2 + 1] = pascalTriangleArray[idx2+1].add(pascalTriangleArray[idx2]);
        }

    }

    private void InPlaceExpander(int idxPascalLevel, BigInteger[] arrayToExpand){

        if (idxPascalLevel%2 == 0){
            arrayToExpand[idxPascalLevel/2] = arrayToExpand[(idxPascalLevel-1)/2].multiply(BigInteger.TWO);
        }

        //Filling this in backwards
        for (int idx2 = (idxPascalLevel - 1)/2 - 1; idx2 >= 0 ; idx2--){
            arrayToExpand[idx2 + 1] = arrayToExpand[idx2+1].add(arrayToExpand[idx2]);
        }
    }

    public void DivisibilityCounter(int pascalLevel, int divisor){

        divisorPower = (int) Math.log10(divisor);

        if (Math.log10(divisor) != divisorPower){
            throw new Error("Divisor: " + divisor + " not a power of 10!");
        }

        //Step1: invoke populate pascal triangle array
        PopulatePascalTriangleArray(pascalLevel);

        //Build factor index for what two items to send to divisibility checker
        int[] factorIndex = new int[pascalLevel+1];
        for (int idx1 = 0; idx1 <= pascalLevel/2 ; idx1++){
            factorIndex[idx1] = idx1;
            factorIndex[pascalLevel - idx1] = idx1;
        }

        /*
        * Step 2: recursive expansion and divisibility checker
        * purposefully make large currentPascalLevelArray so we can work in-place and no need to allocate more size, or
        create new arrays to store data.
         */
        BigInteger[] currentPascalLevelArray = new BigInteger[(int) ((pascalLevel + 1)/2 + (pascalLevel + 1)%2)];
        currentPascalLevelArray[0] = BigInteger.ONE;

        /*
        If the divisor is 1, automatically count the lead value to total divisors.
        Moreover, in this case, every value in the pyramid counts, no need to run any mechanisms under the hood unless
        you are doing a diagnostic.
         */
        if ( divisor == 1 ){ divisibleCoefficients += (pascalLevel+1)*(pascalLevel+2)/2; }
        else {

            /*
            Run the IsDivisible checker for first index to avoid if statement inside of loop.
            This checks the unique case for the first pascal Level
             */
            BigInteger factor = pascalTriangleArray[factorIndex[1]];
            InPlaceExpander(1, currentPascalLevelArray);

            //!NOTE: because first pascal level would count as 2, Increment divisible coefficients twice!
            if ( IsDivisible(currentPascalLevelArray[0], factor) ){ AddTwoDivisibleCoefficientsCounter(); }

            //Now for rest of levels
            for (int idxLevel = 2; idxLevel <= pascalLevel; idxLevel++) {

                //Step 3: fix factor. better to store or better for high volumes of calls?
                factor = pascalTriangleArray[factorIndex[idxLevel]];

                //Step 4: expand in place
                InPlaceExpander(idxLevel, currentPascalLevelArray);

                //Step 5: call divisibility checker
                for (int idxArray = 0; idxArray <= idxLevel/2 - 1; idxArray++) {
                    if (IsDivisible(currentPascalLevelArray[idxArray], factor) ) { AddTwoDivisibleCoefficientsCounter(); }
                }

                // Step 6: even-odd pascalIndex: Odd increment by 2, even increment by 1;
                if (IsDivisible(currentPascalLevelArray[idxLevel/2], factor) ){
                    if ( idxLevel%2==1 ) { AddTwoDivisibleCoefficientsCounter(); }
                    else { AddOneDivisibleCoefficientsCounter(); }
                }
            }
        }
    }

    public void DivisibilityCounterLargePower(int pascalLevel, int tenPower){

        if (tenPower < 1){
            throw new Error("Power: " + tenPower + " has to be integer at least 1!");
        }

        divisorPower = tenPower;

        //DIAG
        final long startTime = System.currentTimeMillis();

        //Step1: invoke populate pascal triangle array
        PopulatePascalTriangleArray(pascalLevel);

        //DIAG
        final long endTime = System.currentTimeMillis();
        System.out.println("PopulatePascalTriangleArray execution time (milliSeconds): " + (endTime - startTime));

        //Build factor index for what two items to send to divisibility checker
        int[] factorIndex = new int[pascalLevel+1];
        for (int idx1 = 0; idx1 <= pascalLevel/2 ; idx1++){
            factorIndex[idx1] = idx1;
            factorIndex[pascalLevel - idx1] = idx1;
        }

        /*
        * Step 2: recursive expansion and divisibility checker
        * purposefully make large currentPascalLevelArray so we can work in-place and no need to allocate more size, or
        create new arrays to store data.
         */
        //DIAG
        final long startTime2 = System.currentTimeMillis();

        BigInteger[] currentPascalLevelArray = new BigInteger[(int) ((pascalLevel + 1)/2 + (pascalLevel + 1)%2)];
        currentPascalLevelArray[0] = BigInteger.ONE;

        /*
        Run the IsDivisible checker for first index to avoid if statement inside of loop.
        This checks the unique case for the first pascal Level
         */
        BigInteger factor = pascalTriangleArray[factorIndex[1]];
        InPlaceExpander(1, currentPascalLevelArray);

        //Now for rest of levels
        for (int idxLevel = 2; idxLevel <= pascalLevel; idxLevel++) {

            //Step 3: fix factor. better to store or better for high volumes of calls?
            factor = pascalTriangleArray[factorIndex[idxLevel]];

            //Step 4: expand in place
            InPlaceExpander(idxLevel, currentPascalLevelArray);

            //Step 5: call divisibility checker
            for (int idxArray = 0; idxArray <= idxLevel/2 - 1; idxArray++) {
                if (IsDivisible(currentPascalLevelArray[idxArray], factor) ) { AddTwoDivisibleCoefficientsCounter(); }
            }

            // Step 6: even-odd pascalIndex: Odd increment by 2, even increment by 1;
            if (IsDivisible(currentPascalLevelArray[idxLevel/2], factor) ){
                if ( idxLevel%2==1 ) { AddTwoDivisibleCoefficientsCounter(); }
                else { AddOneDivisibleCoefficientsCounter(); }
            }
        }

        //DIAG
        final long endTime2 = System.currentTimeMillis();
        System.out.println("PopulatePascalTriangleArray execution time (milliSeconds): " + (endTime2 - startTime2));
    }

    private boolean IsDivisible(BigInteger factor1, BigInteger factor2){

        //count the zeros
        int zeros1 = LaggingZeroCounter(factor1.toString());
        int zeros2 = LaggingZeroCounter(factor2.toString());
        int total = zeros1 + zeros2;

        //Decide if divisible
        if ( total >= divisorPower ) { return true; }
        else {
            //to make this case efficient do like this:
            if ( total + 1 == divisorPower ) {
                //This will rarely happen. Front-load the easy calculation to avoid doing anything more expensive.

                int factor1Lead = Integer.valueOf(String.valueOf(factor1.toString().charAt(factor1.toString().length() - zeros1 - 1)));
                int factor2Lead = Integer.valueOf(String.valueOf(factor2.toString().charAt(factor2.toString().length() - zeros2 - 1)));

                if ( factor1Lead*factor2Lead%10 == 0 ) { return true; }
                else { return false; }
            }
            else { return false; }
        }
    }

    private int LaggingZeroCounter(String valueAsString){
        int indexCounter = 0, zeroCounter = 0;
        while (valueAsString.charAt(valueAsString.length() - indexCounter - 1)=='0'){
            zeroCounter++;
            indexCounter++;
        }
        return zeroCounter;
    }

}
