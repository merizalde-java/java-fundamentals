/*
Auth: Jaime Lopez-Merizalde, Ph.D.
Date: 9 Feb 21
UpD: 11 Feb 21
Descri: basic calculations for mixed type array list
 */

import java.math.BigInteger;

public class MixedTypeCalculator{

    /*
    Methods
     */

    /*
    Addition.
    Notes: core methods work on the first argument being broader than the second argument.
    Overloaded methods will reverse the order for appropriate calculations.
     */

    //Ints
    public Object MathAdd(int leftValue, int rightValue){

        //Diag:
        //System.out.println("hit int + int");

        //leftValue + rightValue <= integerCapacity
        if (((long) leftValue + (long) rightValue ) <= Integer.MAX_VALUE){
            return leftValue + rightValue;
        }
        else {
            return MathAdd((long) leftValue, (long) rightValue);
        }
    }

    //Longs
    public Object MathAdd(long leftValue, long rightValue){

        //diag:
//        System.out.println("Hit long + long");

        BigInteger leftValueAsBigInteger = BigInteger.valueOf(leftValue);
        BigInteger rightValueAsBigInteger = BigInteger.valueOf(rightValue);

        //leftValue + rightValue <= longCapacity
        if ( leftValueAsBigInteger.add(rightValueAsBigInteger).compareTo(BigInteger.valueOf(Long.MAX_VALUE)) <= 0  ){
            return leftValue + rightValue;
        }
        else {
            return MathAdd(leftValueAsBigInteger, rightValueAsBigInteger);
        }
    }

    //MTs (Big Integer + int)
    // Note: In the case of a mixed value, with an integer (or long),
    // we will cast the sum to a BigInteger
    public Object MathAdd(int leftValue, BigInteger rightValue){
        //diag
        //System.out.println("Hit int + bigint");

        return MathAdd(rightValue, leftValue);
    }

    public Object MathAdd(BigInteger leftValue, int rightValue){
        //diag
        //System.out.println("Hit bigint + int");
        return MathAdd(leftValue,BigInteger.valueOf(rightValue));
    }

    public Object MathAdd(long leftValue, BigInteger rightValue){
        //diag
        //System.out.println("Hit long + bigint");
        return MathAdd(rightValue, leftValue);
    }

    public Object MathAdd(BigInteger leftValue, long rightValue){
        //diag
        //System.out.println("Hit bigint + long");
        return MathAdd(leftValue, BigInteger.valueOf(rightValue));
    }

    //Big Integers
    public Object MathAdd(BigInteger leftValue, BigInteger rightValue){
                return leftValue.add(rightValue);
    }

    //Objects
    public Object MathAdd(Object leftValue, Object rightValue){

        /*
        Types: "Integer" "Long" "BigInteger"
         */

        String leftValueSimpleName = leftValue.getClass().getSimpleName();
        String rightValueSimpleName = rightValue.getClass().getSimpleName();

        //Decode the data type
        if (leftValueSimpleName.equals("Integer")
                && rightValueSimpleName.equals("Integer") ){
            return MathAdd((int) leftValue, (int) rightValue);
        }
        else if (leftValueSimpleName.equals("Integer")
                && rightValueSimpleName.equals("Long")){
            //note: there is no int + long, but JAVA will automatically cast to
            //the correct method call: long + long
            return MathAdd((int) leftValue, (long) rightValue);
        }
        else if (leftValueSimpleName.equals("Integer")
                && rightValueSimpleName.equals("BigInteger")){
            return MathAdd((int) leftValue, (BigInteger) rightValue);
        }
        else if (leftValueSimpleName.equals("Long")
                && rightValueSimpleName.equals("Integer")){
            return MathAdd((long) leftValue, (int) rightValue);
        }
        else if (leftValueSimpleName.equals("Long")
                && rightValueSimpleName.equals("Long")){
            return MathAdd((long) leftValue, (long) rightValue);
        }
        else if (leftValueSimpleName.equals("Long")
                && rightValueSimpleName.equals("BigInteger")){
            return MathAdd((long) leftValue, (BigInteger) rightValue);
        }
        else if (leftValueSimpleName.equals("BigInteger")
                && rightValueSimpleName.equals("Integer")){
            return MathAdd((BigInteger) leftValue, (int) rightValue);
        }
        else if (leftValueSimpleName.equals("BigInteger")
                && rightValueSimpleName.equals("Long")){
            return MathAdd((BigInteger) leftValue, (long) rightValue);
        }
        else if (leftValueSimpleName.equals("BigInteger")
                && rightValueSimpleName.equals("BigInteger")){
            return MathAdd((BigInteger) leftValue, (BigInteger) rightValue);
        }
        else {
            System.out.println("Improper Class Type: Left Value: " + leftValueSimpleName
                    + " Right Value: " + rightValueSimpleName);
            return new Object();
        }

    }

    /*
    Multiplication.
    Notes: core methods work on the first argument being broader than the second argument.
    Overloaded methods will reverse the order for appropriate calculations.
     */
    public Object MathMultiply(int leftValue, int rightValue){

        //diag:
        //System.out.println("Hit int*int");

        //recast
        long leftValueAsLong = (long) leftValue;
        long rightValueAsLong = (long) rightValue;

        /*
        leftValue * rightValue <= integerCapacity
         */
        if ( leftValueAsLong*rightValueAsLong <= Integer.MAX_VALUE){
            return leftValue*rightValue;
        }
        else {
            return MathMultiply(leftValueAsLong, rightValueAsLong);
        }
    }

    public Object MathMultiply(long leftValue, long rightValue){

        //diag:
        //System.out.println("Hit long*long");

        //recast
        BigInteger leftValueAsBigInteger = BigInteger.valueOf(leftValue);
        BigInteger rightValueAsBigInteger = BigInteger.valueOf(rightValue);

        /*
        leftValue * rightValue <= longCapacity
         */
        if ( leftValueAsBigInteger.multiply(rightValueAsBigInteger).compareTo(BigInteger.valueOf(Long.MAX_VALUE)) <=0 ){
            return leftValue*rightValue;
        }
        else {
            return MathMultiply(leftValueAsBigInteger, rightValueAsBigInteger);
        }
    }

    public Object MathMultiply(int leftValue, BigInteger rightValue){

        //diag
        //System.out.print("Hit int * BigInteger");
        return MathMultiply(rightValue, leftValue);
    }

    public Object MathMultiply(BigInteger leftValue, int rightValue){
        //diag:
        //System.out.println("Hit BigInteger*int");

        return MathMultiply(leftValue, BigInteger.valueOf(rightValue));
    }

    public Object MathMultiply(long leftValue, BigInteger rightValue){
        //diag:
        //System.out.println("Hit long*BigInt");
        return MathMultiply(rightValue, leftValue);
    }

    public Object MathMultiply(BigInteger leftValue, long rightValue){
        //diag:
        //System.out.println("Hit BigInt*long");
        return MathMultiply(leftValue, BigInteger.valueOf(rightValue));
    }

    public Object MathMultiply(BigInteger leftValue, BigInteger rightValue){
        //diag:
        //System.out.println("Hit BigInt*BigInt");
        return leftValue.multiply(rightValue);
    }

    public Object MathMultiply(Object leftValue, Object rightValue){
        /*
        Types: "Integer" "Long" "BigInteger"
         */

        String leftValueSimpleName = leftValue.getClass().getSimpleName();
        String rightValueSimpleName = rightValue.getClass().getSimpleName();

        //Decode the data type
        if (leftValueSimpleName.equals("Integer")
                && rightValueSimpleName.equals("Integer") ){
            return MathMultiply((int) leftValue, (int) rightValue);
        }
        else if (leftValueSimpleName.equals("Integer")
                && rightValueSimpleName.equals("Long")){
            //note: there is no int + long, but JAVA will automatically cast to
            //the correct method call: long + long
            return MathMultiply((int) leftValue, (long) rightValue);
        }
        else if (leftValueSimpleName.equals("Integer")
                && rightValueSimpleName.equals("BigInteger")){
            return MathMultiply((int) leftValue, (BigInteger) rightValue);
        }
        else if (leftValueSimpleName.equals("Long")
                && rightValueSimpleName.equals("Integer")){
            return MathMultiply((long) leftValue, (int) rightValue);
        }
        else if (leftValueSimpleName.equals("Long")
                && rightValueSimpleName.equals("Long")){
            return MathMultiply((long) leftValue, (long) rightValue);
        }
        else if (leftValueSimpleName.equals("Long")
                && rightValueSimpleName.equals("BigInteger")){
            return MathMultiply((long) leftValue, (BigInteger) rightValue);
        }
        else if (leftValueSimpleName.equals("BigInteger")
                && rightValueSimpleName.equals("Integer")){
            return MathMultiply((BigInteger) leftValue, (int) rightValue);
        }
        else if (leftValueSimpleName.equals("BigInteger")
                && rightValueSimpleName.equals("Long")){
            return MathMultiply((BigInteger) leftValue, (long) rightValue);
        }
        else if (leftValueSimpleName.equals("BigInteger")
                && rightValueSimpleName.equals("BigInteger")){
            return MathMultiply((BigInteger) leftValue, (BigInteger) rightValue);
        }
        else {
            System.out.println("Improper Class Type: Left Value: " + leftValueSimpleName
                    + " Right Value: " + rightValueSimpleName);
            return new Object();
        }
    }

}
